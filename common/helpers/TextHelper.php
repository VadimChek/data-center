<?php

namespace common\helpers;

/**
 * Class TextHelper
 * @package common\helpers
 */
class TextHelper
{
    /**
     * @param string $text
     * @param int $length
     * @param int $start
     * @param bool $withDots
     *
     * @return string
     */
    public static function getShortText(string $text, int $length = 50, int $start = 0, bool $withDots = true): string
    {
        if (strlen($text) <= ($length - 3)) {
            return $text;
        }

        $text = mb_strcut($text, $start, $length);
        if ($withDots) {
            $text .= '...';
        }

        return $text;
    }
}
