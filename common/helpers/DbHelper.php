<?php

namespace common\helpers;

use Yii;
use Exception;

/**
 * Class DbHelper
 * @package common\helpers
 */
class DbHelper
{
    /**
     * @param callable $function
     *
     * @return mixed
     * @throws \yii\db\Exception
     */
    public static function transactional(callable $function)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $result = call_user_func($function);
            $transaction->commit();
        } catch (Exception $ex) {
            try {
                $transaction->rollBack();
            } catch (Exception $e) {
            }

            Yii::error([
                'error' => $ex->getMessage(),
            ]);

            throw $ex;
        }

        return $result;
    }
}
