<?php

namespace common\models\statusHistory;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[StatusHistory]].
 *
 * @see StatusHistory
 */
class StatusHistoryQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return StatusHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return StatusHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
