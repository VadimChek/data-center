<?php

namespace common\models\statusHistory;

use common\models\user\User;
use common\models\bid\Bid;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "status_history".
 *
 * @property int $id
 * @property int $bid_id
 * @property int $user_id
 * @property string $status_from
 * @property string $status_to
 * @property string $created_at
 *
 * @property Bid $bid
 * @property User $user
 */
class StatusHistory extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'status_history';
    }

    /**
     * @return StatusHistoryQuery
     */
    public static function find(): StatusHistoryQuery
    {
        return new StatusHistoryQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['bid_id', 'user_id', 'status_from', 'status_to'], 'required'],
            [['bid_id', 'user_id'], 'integer'],
            [['status_from', 'status_to'], 'string', 'max' => 255],
            [
                ['bid_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Bid::class,
                'targetAttribute' => ['bid_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'bid_id' => 'Заявка',
            'user_id' => 'Пользователь',
            'status_from' => 'с',
            'status_to' => 'по',
            'created_at' => 'Изменен в',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getBid(): ActiveQuery
    {
        return $this->hasOne(Bid::class, ['id' => 'bid_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
