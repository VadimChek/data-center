<?php

namespace common\models\subdivision;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Subdivision]].
 *
 * @see Subdivision
 */
class SubdivisionQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return Subdivision[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Subdivision|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
