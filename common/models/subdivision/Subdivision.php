<?php

namespace common\models\subdivision;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "subdivision".
 *
 * @property int $id ID
 * @property string $name Название подразделения
 * @property int $priority Приоритет в списке
 * @property int $status статус
 * @property boolean $is_important важность
 * @property string $created_at Создано в
 * @property string $updated_at Обновлено в
 */
class Subdivision extends ActiveRecord
{
    const STATUS_ACTIVE = 5;
    const STATUS_ARCHIVE = 10;

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'subdivision';
    }

    /**
     * @return SubdivisionQuery
     */
    public static function find(): SubdivisionQuery
    {
        return new SubdivisionQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getStatusLabels(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активно',
            self::STATUS_ARCHIVE => 'В архиве',
        ];
    }

    /**
     * @return array
     */
    public static function getListForSelect(): array
    {
        $subdivisions = self::find()
            ->select(['id', 'name'])
            ->andWhere(['<>', 'status', self::STATUS_ARCHIVE])
            ->orderBy('priority')
            ->asArray()
            ->all();

        return ArrayHelper::map($subdivisions, 'id', 'name');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getStatusLabel(): string
    {
        return ArrayHelper::getValue(self::getStatusLabels(), $this->status);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'priority' => 'Приоритет в списке',
            'status' => 'Статус',
            'created_at' => 'Создан в',
            'updated_at' => 'Обновлен в',
        ];
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function isArchive(): bool
    {
        return $this->status == self::STATUS_ARCHIVE;
    }
}
