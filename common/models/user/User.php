<?php

namespace common\models\user;

use common\models\profile\Profile;
use common\models\rbac\AuthItem;
use common\models\department\Department;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\web\Link;
use yii\web\Linkable;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $firstname
 * @property string $secondname
 * @property string $lastname
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password
 * @property string $access_token
 * @property string $department_id
 *
 * @property Profile $profile
 * @property Department $department
 */
class User extends ActiveRecord implements IdentityInterface, Linkable
{
    const STATUS_ACTIVE = 10;
    const STATUS_DELETED = 15;

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'user';
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['username', 'firstname', 'secondname', 'lastname'], 'filter', 'filter' => 'trim'],
            [['username', 'firstname', 'secondname', 'lastname'], 'filter', 'filter' => 'strip_tags'],
            [['username', 'auth_key', 'password_hash'], 'required'],
            [['username', 'password_reset_token'], 'unique'],
            [
                [
                    'username', 'password_hash', 'password_reset_token', 'firstname', 'secondname', 'lastname'
                ], 'string', 'max' => 255
            ],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['auth_key'], 'string', 'max' => 32],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Пароль',
            'password_reset_token' => 'Токен сброса пароля',
            'status' => 'Статус',
            'firstname' => 'Имя',
            'secondname' => 'Фамилия',
            'lastname' => 'Отчество',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel(): string
    {
        return self::getStatusLabels()[$this->status];
    }

    /**
     * @return array
     */
    public static function getStatusLabels(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DELETED => 'Удален',
        ];
    }

    /**
     * @param int|string $id
     *
     * @return User|IdentityInterface|null
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @param string $username
     *
     * @return User|null
     */
    public static function findByUsername(string $username): ?User
    {
        return User::findOne([
            'username' => $username,
            'status' => User::STATUS_ACTIVE,
        ]);
    }

    /**
     * @param mixed $token
     * @param null $type
     *
     * @return IdentityInterface|null
     */
    public static function findIdentityByAccessToken($token, $type = null): ?IdentityInterface
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @param string $token
     *
     * @return User|null
     */
    public static function findByPasswordResetToken(string $token): ?User
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'status' => self::STATUS_ACTIVE,
            'password_reset_token' => $token,
        ]);
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    public static function isPasswordResetTokenValid(string $token): bool
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * @return array|int|mixed|string|null
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @return string
     */
    public function getAuthKey(): string
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     *
     * @return bool
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param $password
     *
     * @return bool
     */
    public function validatePassword($password): bool
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @param $password
     *
     * @throws \yii\base\Exception
     */
    public function setPassword($password): void
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function generateAuthKey(): void
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @throws \yii\base\Exception
     */
    public function generatePasswordResetToken(): void
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * @return void
     */
    public function removePasswordResetToken(): void
    {
        $this->password_reset_token = null;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return [
            Link::REL_SELF => Url::to(['user/view', 'id' => $this->id], true),
        ];
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            'username',
            'status',
            'created_at' => function () {
                return date('Y-m-d', strtotime($this->created_at));
            },
            'updated_at' => function () {
                return date('Y-m-d', strtotime($this->updated_at));
            },
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProfile(): ActiveQuery
    {
        return $this->hasOne(Profile::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDepartment(): ActiveQuery
    {
        return $this->hasOne(Department::class, ['id' => 'department_id']);
    }

    /**
     * @return string
     */
    public function getFio(): string
    {
        return h($this->secondname . ' ' . $this->firstname . ' ' . $this->lastname);
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return Yii::$app->authManager->checkAccess($this->id, AuthItem::ROLE_ADMIN);
    }

    /**
     * @return bool
     */
    public function isOperator(): bool
    {
        return Yii::$app->authManager->checkAccess($this->id, AuthItem::ROLE_OPERATOR);
    }

    /**
     * @return bool
     */
    public function isHead(): bool
    {
        return Yii::$app->authManager->checkAccess($this->id, AuthItem::ROLE_HEAD);
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->status == self::STATUS_DELETED;
    }

    /**
     * @return bool
     */
    public function inDepartmentCod(): bool
    {
        return $this->department && $this->department->name == Department::COD;
    }

    /**
     * @return string|null
     */
    public function getRole(): ?string
    {
        if ($this->isAdmin()) {
            return AuthItem::ROLE_ADMIN;
        } elseif ($this->isHead()) {
            return AuthItem::ROLE_HEAD;
        } elseif ($this->isOperator()) {
            return AuthItem::ROLE_OPERATOR;
        } else {
            return null;
        }
    }
}
