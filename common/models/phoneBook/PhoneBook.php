<?php

namespace common\models\phoneBook;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "phone_book".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $mobile
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class PhoneBook extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'phone_book';
    }

    /**
     * @return PhoneBookQuery
     */
    public static function find(): PhoneBookQuery
    {
        return new PhoneBookQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getAllPhones(): array
    {
        return self::find()
            ->active()
            ->orderBy(['position' => SORT_DESC])
            ->all();
    }
}
