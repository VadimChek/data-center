<?php

namespace common\models\phoneBook;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[PhoneBook]].
 *
 * @see PhoneBook
 */
class PhoneBookQuery extends ActiveQuery
{
    /**
     * @param string|null $alias
     *
     * @return PhoneBookQuery
     */
    public function active(string $alias = null): PhoneBookQuery
    {
        return $this->andWhere([($alias ? "{$alias}." : '') . 'deleted_at' => null]);
    }

    /**
     * {@inheritdoc}
     * @return PhoneBook[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PhoneBook|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
