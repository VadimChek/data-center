<?php

namespace common\models\rbac;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "auth_rule".
 *
 * @property string $name
 * @property resource $data
 * @property int $created_at
 * @property int $updated_at
 *
 * @property AuthItem[] $authItems
 */
class AuthRule extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'auth_rule';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['data'], 'string'],
            [['created_at', 'updated_at'], 'default', 'value' => null],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['name'], 'unique'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'name' => 'Название',
            'data' => 'Дата',
            'created_at' => 'Создано в',
            'updated_at' => 'Обновлено в',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAuthItems(): ActiveQuery
    {
        return $this->hasMany(AuthItem::class, ['rule_name' => 'name']);
    }
}
