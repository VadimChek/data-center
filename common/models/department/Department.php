<?php

namespace common\models\department;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "department".
 *
 * @property int $id
 * @property string $name
 */
class Department extends ActiveRecord
{
    const COD = 'ЦОД';
    const ITC = 'ИТЦ';

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'department';
    }

    /**
     * @return DepartmentQuery
     */
    public static function find(): DepartmentQuery
    {
        return new DepartmentQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @return string[]
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @param array $excluded
     * @return array
     */
    public static function getAllList(array $excluded = []): array
    {
        $query = self::find()
            ->select(['id', 'name'])
            ->asArray();

        if ($excluded) {
            $query->andWhere(['not in', 'name', $excluded]);
        }

        return ArrayHelper::map($query->all(), 'id', 'name');
    }

    /**
     * @param $departmentId
     *
     * @return string
     * @throws \Exception
     */
    public static function getName($departmentId): string
    {
        return ArrayHelper::getValue(self::getAllList(), $departmentId, 'Без отдела');
    }
}
