<?php

namespace common\models\bid;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Bid]].
 *
 * @see Bid
 */
class BidQuery extends ActiveQuery
{
    /**
     * @param string|null $alias
     *
     * @return BidQuery
     */
    public function active(string $alias = null): BidQuery
    {
        return $this->andWhere([($alias ? "{$alias}." : '') . 'status' => Bid::STATUS_ACTIVE]);
    }

    /**
     * @param int $departmentId
     * @param string|null $alias
     *
     * @return BidQuery
     */
    public function byDepartmentId(int $departmentId, string $alias = null): BidQuery
    {
        return $this->andWhere([($alias ? "{$alias}." : '') . 'department_id' => $departmentId]);
    }

    /**
     * {@inheritdoc}
     * @return Bid[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Bid|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
