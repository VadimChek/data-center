<?php

namespace common\models\bid;

use common\models\rbac\AuthItem;
use common\models\department\Department;
use common\models\statusHistory\StatusHistory;
use common\models\subdivision\Subdivision;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bid".
 *
 * @property int $id ID
 * @property int $number Номер по порядку
 * @property string $accepted_user Кто принял заявку
 * @property string $applicant_user Заявитель
 * @property string $department_id Отдел
 * @property string $subdivision_id Подразделение
 * @property string $content Содержание заявки
 * @property string $note Примечание
 * @property int $building Корпус
 * @property int $floor Этаж
 * @property string $room Помещение
 * @property string $phone Телефон
 * @property int $status Статус
 * @property string $date Дата
 * @property string $date_complete Дата выполнения
 * @property boolean $is_visible Видимость
 * @property string $created_at Создано в
 * @property string $updated_at Обновлено в
 *
 * @property Department $department
 * @property Subdivision $subdivision
 * @property StatusHistory $statusHistory
 */
class Bid extends ActiveRecord
{
    const STATUS_IN_WORK = 3;
    const STATUS_ACTIVE = 5;
    const STATUS_COMPLETE = 10;
    const STATUS_NOT_COMPLETE = 11;
    const STATUS_ARCHIVE = 15;

    const STATUS_ALL = 999;

    const VISIBLE = 1;
    const NOT_VISIBLE = 2;
    const ALL = 3;

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'bid';
    }

    /**
     * @return BidQuery
     */
    public static function find(): BidQuery
    {
        return new BidQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getVisibleLabels(): array
    {
        return [
            self::VISIBLE => 'Видимые',
            self::NOT_VISIBLE => 'Скрытые',
            self::ALL => 'Все',
        ];
    }

    /**
     * @return array
     */
    public static function getStatusLabels(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активна',
            self::STATUS_IN_WORK => 'В работе',
            self::STATUS_COMPLETE => 'Исполнена',
            self::STATUS_NOT_COMPLETE => 'Не исполнена',
            self::STATUS_ARCHIVE => 'В архиве',
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getStatusLabel(): string
    {
        return ArrayHelper::getValue(self::getStatusLabels(), $this->status, 'не определен');
    }

    /**
     * @return array
     */
    public static function getRbacStatusLabels(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активна',
            self::STATUS_IN_WORK => 'В работе',
            self::STATUS_COMPLETE => 'Исполнена',
            self::STATUS_NOT_COMPLETE => 'Не исполнена',
        ];
    }

    /**
     * @return array
     */
    public static function getRbacStatusLabelsForAdmin(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активна',
            self::STATUS_IN_WORK => 'В работе',
            self::STATUS_COMPLETE => 'Исполнена',
            self::STATUS_NOT_COMPLETE => 'Не исполнена',
            self::STATUS_ARCHIVE => 'В архиве',
        ];
    }

    /**
     * @return array
     */
    public static function getFilterStatusLabels(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активна',
            self::STATUS_IN_WORK => 'В работе',
            self::STATUS_COMPLETE => 'Исполнена',
            self::STATUS_NOT_COMPLETE => 'Не исполнена',
            self::STATUS_ARCHIVE => 'В архиве',
            self::STATUS_ALL => 'Все',
        ];
    }

    /**
     * @return array
     */
    public static function getFloors(): array
    {
        $arr = range(1, 10);
        return array_combine($arr, $arr);
    }

    /**
     * @return false|string|null
     */
    public static function getLastAcceptedUser()
    {
        return self::find()
            ->select('accepted_user')
            ->orderBy(['created_at' => SORT_DESC])
            ->asArray()
            ->scalar();
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['number', 'content', 'accepted_user', 'department_id', 'subdivision_id', 'applicant_user'], 'required'],

            [['building'], 'integer', 'min' => 1, 'max' => 20],
            [['floor'], 'integer', 'min' => 1, 'max' => 10],

            [['number', 'status', 'department_id', 'subdivision_id'], 'integer'],
            [['content', 'note', 'room'], 'string'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['accepted_user', 'applicant_user', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'number' => 'Номер по порядку',
            'accepted_user' => 'Кто принял заявку',
            'applicant_user' => 'Заявитель',
            'department_id' => 'Ответственный',
            'subdivision_id' => 'Подразделение ВИТ ЭРА',
            'content' => 'Содержание заявки',
            'note' => 'Примечание',
            'building' => 'Корпус',
            'floor' => 'Этаж',
            'room' => 'Помещение',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'date' => 'Дата подачи',
            'created_at' => 'Создана в',
            'updated_at' => 'Обновлена в',
        ];
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function isInWork(): bool
    {
        return $this->status == self::STATUS_IN_WORK;
    }

    /**
     * @return bool
     */
    public function isComplete(): bool
    {
        return $this->status == self::STATUS_COMPLETE;
    }

    /**
     * @return bool
     */
    public function isNotComplete(): bool
    {
        return $this->status == self::STATUS_NOT_COMPLETE;
    }

    /**
     * @return bool
     */
    public function isArchive(): bool
    {
        return $this->status == self::STATUS_ARCHIVE;
    }

    /**
     * @return bool
     */
    public function isImportant(): bool
    {
        return $this->subdivision && $this->subdivision->is_important;
    }

    /**
     * @return ActiveQuery
     */
    public function getDepartment(): ActiveQuery
    {
        return $this->hasOne(Department::class, ['id' => 'department_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSubdivision(): ActiveQuery
    {
        return $this->hasOne(Subdivision::class, ['id' => 'subdivision_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStatusHistory(): ActiveQuery
    {
        return $this->hasMany(StatusHistory::class, ['bid_id' => 'id'])
            ->orderBy(['created_at' => SORT_DESC]);
    }
}
