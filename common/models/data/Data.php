<?php

namespace common\models\data;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "data".
 *
 * @property int $id
 * @property string $key
 * @property string $content
 */
class Data extends ActiveRecord
{
    const NOTE_KEY = 'note-key';

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'data';
    }

    /**
     * @return DataQuery
     */
    public static function find(): DataQuery
    {
        return new DataQuery(get_called_class());
    }

    /**
     * @param string $key
     *
     * @return Data|null
     */
    public static function getByKey(string $key): ?Data
    {
        return self::findOne(['key' => $key]);
    }

    /**
     * @param string $key
     *
     * @return string
     */
    public static function getValueByKey(string $key): string
    {
        $data = self::getByKey($key);
        return $data ? $data->content : '';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['key'], 'required'],
            [['content'], 'string'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'key' => 'Ключ',
            'content' => 'Значение',
        ];
    }
}
