<?php

namespace common\models\event;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Events]].
 *
 * @see Events
 */
class EventsQuery extends ActiveQuery
{
    /**
     * @param string|null $alias
     *
     * @return EventsQuery
     */
    public function active(string $alias = null): EventsQuery
    {
        return $this->andWhere([($alias ? "{$alias}." : '') . 'deleted_at' => null]);
    }

    /**
     * @param string $date
     * @param string|null $alias
     *
     * @return EventsQuery
     */
    public function byDate(string $date, string $alias = null): EventsQuery
    {
        return $this->andWhere([($alias ? "{$alias}." : '') . 'date' => $date]);
    }

    /**
     * {@inheritdoc}
     * @return Events[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Events|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
