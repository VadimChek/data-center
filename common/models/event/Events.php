<?php

namespace common\models\event;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property string $date
 * @property string $name
 * @property integer $type
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Events extends ActiveRecord
{
    const TYPE_BIRTHDAY = 5;

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'events';
    }

    /**
     * @return EventsQuery
     */
    public static function find(): EventsQuery
    {
        return new EventsQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getTypeLabels(): array
    {
        return [
            self::TYPE_BIRTHDAY => 'День рождения',
        ];
    }
}
