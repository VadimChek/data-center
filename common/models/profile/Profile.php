<?php

namespace common\models\profile;

use common\models\user\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "profile".
 *
 * @property int $id ID
 * @property int $user_id Пользователь
 * @property string $phone Номер телефона
 * @property boolean $is_read_notes Прочитаны ли заметки
 *
 * @property User $user
 */
class Profile extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'profile';
    }

    /**
     * @return ProfileQuery
     */
    public static function find(): ProfileQuery
    {
        return new ProfileQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['user_id'], 'required'],
            [['user_id',], 'integer'],
            [['phone'], 'string', 'max' => 255],
            [['is_read_notes'], 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'phone' => 'Номер телефона',
            'is_read_notes' => 'Прочитаны заметки',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return bool
     */
    public function isReadNotes(): bool
    {
        return $this->is_read_notes;
    }
}
