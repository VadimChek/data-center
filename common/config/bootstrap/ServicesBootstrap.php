<?php

namespace common\config\bootstrap;

use common\services\bid\BidService;
use common\services\BidServiceInterface;
use common\services\core\SoftDeleteService;
use common\services\phoneBook\PhoneBookService;
use common\services\PhoneBookServiceInterface;
use common\services\profile\ProfileService;
use common\services\ProfileServiceInterface;
use common\services\SoftDeleteServiceInterface;
use common\services\statusHistory\StatusHistoryService;
use common\services\StatusHistoryServiceInterface;
use common\services\subdivision\SubdivisionService;
use common\services\SubdivisionServiceInterface;
use common\services\user\UserService;
use common\services\UserServiceInterface;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class ServicesBootstrap
 * @package common\config\bootstrap
 */
class ServicesBootstrap implements BootstrapInterface
{
    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        $container = Yii::$container;

        $container->set(StatusHistoryServiceInterface::class, function () {
            return Yii::createObject(StatusHistoryService::class);
        });

        $container->set(SoftDeleteServiceInterface::class, function () {
            return Yii::createObject(SoftDeleteService::class);
        });

        $container->set(UserServiceInterface::class, function () {
            return Yii::createObject(UserService::class);
        });

        $container->set(BidServiceInterface::class, function () {
            return Yii::createObject(BidService::class);
        });

        $container->set(ProfileServiceInterface::class, function () {
            return Yii::createObject(ProfileService::class);
        });

        $container->set(PhoneBookServiceInterface::class, function () {
            return Yii::createObject(PhoneBookService::class);
        });

        $container->set(SubdivisionServiceInterface::class, function () {
            return Yii::createObject(SubdivisionService::class);
        });
    }
}
