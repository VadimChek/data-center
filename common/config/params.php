<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'noreplyEmail' => 'no-reply@example.com',
    'user.passwordResetTokenExpire' => 3600,

    'id-backend' => 'app-backend',
    'name' => 'Base-App',

    'close-registration' => true,
];
