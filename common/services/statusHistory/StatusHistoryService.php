<?php

namespace common\services\statusHistory;

use common\models\bid\Bid;
use common\models\statusHistory\StatusHistory;
use common\services\StatusHistoryServiceInterface;
use Exception;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;

/**
 * Class StatusHistoryService
 * @package common\services\statusHistory
 */
class StatusHistoryService extends BaseObject implements StatusHistoryServiceInterface
{
    /**
     * @param Bid $bid
     * @param int $userId
     *
     * @return StatusHistory
     * @throws Exception
     */
    public function createFromBid(Bid $bid, int $userId): StatusHistory
    {
        $model = new StatusHistory();
        $newStatus = $bid->getAttribute('status');
        $oldStatus = $bid->getOldAttribute('status');
        if ($oldStatus != $newStatus) {
            $model->bid_id = $bid->id;
            $model->user_id = $userId;
            $model->status_from = ArrayHelper::getValue(Bid::getStatusLabels(), $oldStatus, 'не определен');
            $model->status_to = ArrayHelper::getValue(Bid::getStatusLabels(), $newStatus, 'не определен');
            if (!$model->save()) {
                throw new Exception('Ошибка сохранения лога о смене статуса заявки');
            }
        }

        return $model;
    }
}
