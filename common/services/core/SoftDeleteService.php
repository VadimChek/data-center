<?php

namespace common\services\core;

use DomainException;
use common\services\SoftDeleteServiceInterface;
use yii\base\BaseObject;
use yii\base\Model;
use yii\web\NotFoundHttpException;

/**
 * Class SoftDeleteService
 * @package common\services\core
 */
class SoftDeleteService extends BaseObject implements SoftDeleteServiceInterface
{
    /**
     * @param int $id
     * @param string $className
     *
     * @return Model
     * @throws NotFoundHttpException
     */
    public function softDelete(int $id, string $className): Model
    {
        $model = $className::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('Запись не найдена');
        }

        $model->deleted_at = date('Y-m-d H:i:s');
        if (!$model->save()) {
            throw new DomainException('Ошибка мягкого удаления записи');
        }

        return $model;
    }
}
