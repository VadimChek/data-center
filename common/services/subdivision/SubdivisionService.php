<?php

namespace common\services\subdivision;

use common\models\subdivision\Subdivision;
use common\services\SubdivisionServiceInterface;
use DomainException;
use yii\base\BaseObject;
use yii\web\NotFoundHttpException;

/**
 * Class SubdivisionService
 * @package common\services\subdivision
 */
class SubdivisionService extends BaseObject implements SubdivisionServiceInterface
{
    /**
     * @param int $id
     * @param int $newStatus
     *
     * @return Subdivision
     * @throws NotFoundHttpException
     */
    public function changeStatus(int $id, int $newStatus): Subdivision
    {
        $model = Subdivision::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('Заявка не найдена');
        }

        $model->status = $newStatus;
        if (!$model->save()) {
            throw new DomainException('Ошибка смены статуса подразделения');
        }

        return $model;
    }
}
