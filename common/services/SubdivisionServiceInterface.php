<?php

namespace common\services;

use common\models\subdivision\Subdivision;

/**
 * Interface SubdivisionServiceInterface
 * @package common\services
 */
interface SubdivisionServiceInterface
{
    /**
     * @param int $id
     * @param int $newStatus
     *
     * @return Subdivision
     */
    public function changeStatus(int $id, int $newStatus): Subdivision;
}
