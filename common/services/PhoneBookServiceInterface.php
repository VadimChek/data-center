<?php

namespace common\services;

/**
 * Interface PhoneBookServiceInterface
 * @package common\services
 */
interface PhoneBookServiceInterface
{
    /**
     * @param array $items
     *
     * @return bool
     */
    public function recalculatePosition(array $items): bool;
}
