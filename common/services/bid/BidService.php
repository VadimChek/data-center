<?php

namespace common\services\bid;

use common\helpers\DbHelper;
use common\models\bid\Bid;
use common\services\BidServiceInterface;
use common\services\StatusHistoryServiceInterface;
use DomainException;
use Yii;
use yii\base\BaseObject;
use yii\web\NotFoundHttpException;

/**
 * Class BidService
 * @package common\services\bid
 */
class BidService extends BaseObject implements BidServiceInterface
{
    private $_statusHistoryService;

    /**
     * BidService constructor.
     *
     * @param StatusHistoryServiceInterface $statusHistoryService
     * @param array $config
     */
    public function __construct(StatusHistoryServiceInterface $statusHistoryService, $config = [])
    {
        $this->_statusHistoryService = $statusHistoryService;
        parent::__construct($config);
    }

    /**
     * @param array $ids
     *
     * @return int
     */
    public function hideByIds(array $ids): int
    {
        return Bid::updateAll(['is_visible' => false], ['id' => $ids]);
    }

    /**
     * @param int $id
     * @param int $newStatus
     *
     * @return Bid
     * @throws NotFoundHttpException
     * @throws DomainException
     * @throws \yii\db\Exception
     */
    public function changeStatus(int $id, int $newStatus): Bid
    {
        $model = Bid::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('Заявка не найдена');
        }

        $model->status = $newStatus;
        if ($newStatus === Bid::STATUS_COMPLETE) {
            $model->date_complete = date('Y-m-d H:i:s');
        }

        return DbHelper::transactional(function () use ($model) {
            $this->_statusHistoryService->createFromBid($model, Yii::$app->user->getId());
            if (!$model->save()) {
                throw new DomainException('Ошибка смены статуса заявки');
            }

            return $model;
        });
    }
}
