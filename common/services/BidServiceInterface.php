<?php

namespace common\services;

use common\models\bid\Bid;

/**
 * Interface BidServiceInterface
 * @package common\services
 */
interface BidServiceInterface
{
    /**
     * @param array $ids
     *
     * @return int
     */
    public function hideByIds(array $ids): int;

    /**
     * @param int $id
     * @param int $newStatus
     *
     * @return Bid
     */
    public function changeStatus(int $id, int $newStatus): Bid;
}
