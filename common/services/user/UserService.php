<?php

namespace common\services\user;

use common\models\user\User;
use common\services\UserServiceInterface;
use yii\base\BaseObject;
use yii\web\NotFoundHttpException;

/**
 * Class UserService
 * @package common\services\user
 */
class UserService extends BaseObject implements UserServiceInterface
{
    /**
     * @param int $id
     * @param int $newStatus
     *
     * @return bool
     * @throws NotFoundHttpException
     */
    public function changeStatus(int $id, int $newStatus): bool
    {
        $model = User::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('Пользователь не найден');
        }
        $model->status = $newStatus;

        return $model->save();
    }
}
