<?php

namespace common\services;

use yii\base\Model;

/**
 * Interface SoftDeleteServiceInterface
 * @package common\services
 */
interface SoftDeleteServiceInterface
{
    /**
     * @param int $id
     * @param string $className
     *
     * @return Model
     */
    public function softDelete(int $id, string $className): Model;
}
