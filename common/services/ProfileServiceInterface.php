<?php

namespace common\services;

use common\models\profile\Profile;

/**
 * Interface ProfileServiceInterface
 * @package common\services
 */
interface ProfileServiceInterface
{
    /**
     * @param int $userId
     * @param array $attributes
     *
     * @return Profile
     */
    public function update(int $userId, array $attributes = []): Profile;
}
