<?php

namespace common\services\phoneBook;

use common\helpers\DbHelper;
use common\models\phoneBook\PhoneBook;
use common\services\PhoneBookServiceInterface;
use yii\base\BaseObject;
use yii\db\Exception;

/**
 * Class PhoneBookService
 * @package common\services\phoneBook
 */
class PhoneBookService extends BaseObject implements PhoneBookServiceInterface
{
    /**
     * @param array $items
     *
     * @return bool
     * @throws Exception
     */
    public function recalculatePosition(array $items): bool
    {
        return DbHelper::transactional(function () use ($items) {
            $count = PhoneBook::find()->count();
            foreach ($items as $key => $id) {
                PhoneBook::updateAll(['position' => $count], ['id' => $id]);
                $count--;
            }

            return true;
        });
    }
}
