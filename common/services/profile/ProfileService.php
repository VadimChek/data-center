<?php

namespace common\services\profile;

use common\models\profile\Profile;
use common\services\ProfileServiceInterface;
use DomainException;
use yii\base\BaseObject;
use yii\web\NotFoundHttpException;

/**
 * Class ProfileService
 * @package common\services\profile
 */
class ProfileService extends BaseObject implements ProfileServiceInterface
{
    /**
     * @param int $userId
     * @param array $attributes
     *
     * @return Profile
     * @throws NotFoundHttpException
     */
    public function update(int $userId, array $attributes = []): Profile
    {
        $model = Profile::findOne(['user_id' => $userId]);
        if ($model === null) {
            throw new NotFoundHttpException('Профиль не найден');
        }

        $model->setAttributes($attributes);
        if ($model->save()) {
            return $model;
        }

        throw new DomainException('Ошибка обновления профиля');
    }
}
