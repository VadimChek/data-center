<?php

namespace common\services;

/**
 * Interface UserServiceInterface
 * @package common\services
 */
interface UserServiceInterface
{
    /**
     * @param int $id
     * @param int $newStatus
     *
     * @return bool
     */
    public function changeStatus(int $id, int $newStatus): bool;
}
