<?php

namespace common\services;

use common\models\bid\Bid;
use common\models\statusHistory\StatusHistory;

/**
 * Interface StatusHistoryServiceInterface
 * @package common\services
 */
interface StatusHistoryServiceInterface
{
    /**
     * @param Bid $bid
     * @param int $userId
     *
     * @return StatusHistory
     */
    public function createFromBid(Bid $bid, int $userId): StatusHistory;
}
