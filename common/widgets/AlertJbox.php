<?php

namespace common\widgets;

use Yii;
use yii\bootstrap\Widget;
use yii\helpers\Html;

/**
 * Class AlertJbox
 * @package backend\widgets
 */
class AlertJbox extends Widget
{
    public $alertRel = [
        'error' => 'red',
        'danger' => 'red',
        'success' => 'green',
        'info' => 'blue',
        'warning' => 'yellow'
    ];

    /**
     * @return void
     */
    public function init()
    {
        parent::init();
        $session = Yii::$app->getSession();
        $flashes = $session->getAllFlashes();

        echo '<div id="jbox-container" class="hidden">';
        foreach ($flashes as $type => $data) {
            if (isset($this->alertRel[$type])) {
                $data = (array)$data;
                foreach ($data as $i => $message) {
                    echo $this->renderBody($message, $this->alertRel[$type]);
                }

                $session->removeFlash($type);
            }
        }
        $this->view->registerJs('
          $(".noticeJbox").each(function() {
                $(this).jBox("Notice", {
                    color: $(this).attr("rel"),
                    content: $(this).text(),
                    animation: "flip"
                });
            });
        ');
        echo '</div>';
    }

    /**
     * @param string $message
     * @param $rel
     *
     * @return string
     */
    public function renderBody(string $message, $rel): string
    {
        return Html::tag('div', $message, ['class' => 'noticeJbox', 'rel' => $rel]);
    }
}
