<?php

use yii\mail\MessageInterface;
use yii\web\View;

/**
 * @var View $this
 * @var MessageInterface $message
 * @var string $content
 */
?>

<?php $this->beginPage() ?>
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
<?php $this->endPage() ?>
