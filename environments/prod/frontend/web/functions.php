<?php

use yii\helpers\Html;

function h($text)
{
    return Html::encode($text);
}
