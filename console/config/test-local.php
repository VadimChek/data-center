<?php

return [
    'components' => [
        'db' => [
            'dsn' => 'pgsql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_DATABASE_TEST'),
        ]
    ],
];
