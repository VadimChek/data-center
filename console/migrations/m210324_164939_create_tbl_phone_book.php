<?php

use yii\db\Migration;

class m210324_164939_create_tbl_phone_book extends Migration
{
    public function safeUp()
    {
        $this->createTable('phone_book', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'phone' => $this->string(),
            'mobile' => $this->string(),
            'position' => $this->integer()->defaultValue(0),
            'created_at' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
            'updated_at' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
            'deleted_at' => 'timestamp with time zone DEFAULT NULL',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('phone_book');
    }
}
