<?php

use yii\db\Migration;

class m210213_032826_create_tbl_data extends Migration
{
    public function safeUp()
    {
        $this->createTable('data', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull(),
            'content' => $this->text(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('data');
    }
}
