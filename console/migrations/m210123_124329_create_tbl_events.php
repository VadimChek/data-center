<?php

use yii\db\Migration;

class m210123_124329_create_tbl_events extends Migration
{
    public function safeUp()
    {
        $this->createTable('events', [
            'id' => $this->primaryKey(),
            'name' => $this->string(1024)->notNull(),
            'date' => $this->date()->notNull(),
            'type' => $this->integer()->comment('Тип'),
            'created_at' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
            'updated_at' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
            'deleted_at' => 'timestamp with time zone DEFAULT NULL',
        ]);

        $this->createIndex('idx-events-date', 'events', 'date');
    }

    public function safeDown()
    {
        $this->dropTable('events');
    }
}
