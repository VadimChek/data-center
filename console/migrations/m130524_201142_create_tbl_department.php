<?php

use yii\db\Migration;

class m130524_201142_create_tbl_department extends Migration
{
    public function safeUp()
    {
        $this->createTable('department', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
        ]);

        $this->batchInsert('department', ['name'], [
            ['IT-технологии'],
            ['Мультимедия'],
            ['Телефония'],
            ['ЦОД'],
            ['ИТЦ'],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('department');
    }
}
