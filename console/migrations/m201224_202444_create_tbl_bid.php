<?php

use common\models\bid\Bid;
use yii\db\Migration;

class m201224_202444_create_tbl_bid extends Migration
{
    public function safeUp()
    {
        $this->createTable('bid', [
            'id' => $this->primaryKey(),
            'number' => $this->integer()->notNull()->comment('Номер по порядку'),
            'accepted_user' => $this->string()->comment('Кто принял заявку'),
            'applicant_user' => $this->string()->comment('Заявитель'),
            'department_id' => $this->integer()->defaultValue(null)->comment('Отдел ИТЦ'),
            'subdivision_id' => $this->integer()->defaultValue(null)->comment('Подразделение ВИТ ЭРА'),
            'content' => $this->text()->comment('Содержание заявки'),
            'note' => $this->text()->comment('Примечание'),
            'building' => $this->integer()->comment('Корпус'),
            'floor' => $this->integer()->comment('Этаж'),
            'room' => $this->string()->comment('Помещение'),
            'phone' => $this->string()->comment('Телефон'),
            'is_visible' => $this->boolean()->defaultValue(true),
            'status' => $this->integer()->defaultValue(Bid::STATUS_ACTIVE)->comment('статус'),
            'date' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
            'date_complete' => 'timestamp with time zone DEFAULT NULL',
            'created_at' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
            'updated_at' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
        ]);

        $this->addForeignKey('fk__Bid_departmentId__User_id',
            'bid', 'department_id',
            'department', 'id',
            'RESTRICT'
        );

        $this->addForeignKey('fk__Bid_subdivisionId__Subdivision_id',
            'bid', 'subdivision_id',
            'subdivision', 'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        $this->dropTable('bid');
    }
}
