<?php

use common\models\rbac\AuthItem;
use yii\db\Migration;

class m180829_150013_add_rbac_role extends Migration
{
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        $admin = $auth->createRole(AuthItem::ROLE_ADMIN);
        $admin->description = 'Администратор';

        $expert = $auth->createRole(AuthItem::ROLE_OPERATOR);
        $expert->description = 'Оператор';

        $user = $auth->createRole(AuthItem::ROLE_HEAD);
        $user->description = 'Начальство';

        $auth->add($admin);
        $auth->add($expert);
        $auth->add($user);

        $backend = $auth->createPermission('backend');
        $backend->description = 'Просмотр админки';

        $auth->add($backend);

        $auth->addChild($admin, $backend);
        $auth->assign($admin, 1);
    }

    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}
