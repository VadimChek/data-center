<?php

use yii\db\Migration;

class m130524_201442_create_tbl_user extends Migration
{
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'department_id' => $this->integer()->defaultValue(null),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'firstname' => $this->string()->comment('Имя'),
            'secondname' => $this->string()->comment('Фамилия'),
            'lastname' => $this->string()->comment('Отчество'),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
            'updated_at' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
        ]);

        $this->addForeignKey('fk__User_departmentId__Department_id',
            'user', 'department_id',
            'department', 'id',
            'RESTRICT'
        );

        $this->createTable('profile', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'phone' => $this->string()->comment('Номер телефона'),
            'is_read_notes' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey('fk__Profile_userId__User_id',
            'profile', 'user_id',
            'user', 'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('profile');
        $this->dropTable('user');
    }
}
