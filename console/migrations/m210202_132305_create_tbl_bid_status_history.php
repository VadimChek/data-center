<?php

use yii\db\Migration;

class m210202_132305_create_tbl_bid_status_history extends Migration
{
    public function safeUp()
    {
        $this->createTable('status_history', [
            'id' => $this->primaryKey(),
            'bid_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'status_from' => $this->string()->notNull(),
            'status_to' => $this->string()->notNull(),
            'created_at' => 'timestamp with time zone NOT NULL DEFAULT NOW()',
        ]);

        $this->createIndex('idx__StatusHistory_bidId', 'status_history', 'bid_id');
        $this->createIndex('idx__StatusHistory_userId', 'status_history', 'user_id');

        $this->addForeignKey(
            'fk__StatusHistory_bidId__Bid_id',
            'status_history', 'bid_id',
            'bid', 'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk__StatusHistory_userId__User_id',
            'status_history', 'user_id',
            'user', 'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        $this->dropTable('status_history');
    }
}
