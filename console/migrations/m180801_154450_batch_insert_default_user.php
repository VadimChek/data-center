<?php

use common\models\user\User;
use yii\db\Migration;
use yii\db\Query;

class m180801_154450_batch_insert_default_user extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('user', ['username', 'auth_key', 'password_hash', 'status'], [
            [
                'admin',
                'AIVCSJKKGgy1i3Y8BmSRCN_Cnb7a3D1S',
                '$2y$13$386snSaCvg8DxqpGWXtVNuESHvXNMYWoJKs0Gd1AP/uGWQEc1Q4vO', // passw0rd
                User::STATUS_ACTIVE,
            ]
        ]);

        $userId = (new Query())
            ->from(User::tableName())
            ->select(['id'])
            ->where(['username' => 'admin'])
            ->scalar();

        $this->batchInsert('profile', ['user_id', 'is_read_notes'], [
            [$userId, true]
        ]);
    }

    public function safeDown()
    {
        $this->truncateTable('profile');
        $this->truncateTable('user');
    }
}
