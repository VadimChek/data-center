@echo off

SetLocal EnableExtensions EnableDelayEdExpansion

set file=config.yml

For /F "UseBackQ tokens=1-2 delims==" %%a in ("%file%") do if "%%a"=="user" set user=%%b
echo 1. User - %user%

For /F "UseBackQ tokens=1-2 delims==" %%a in ("%file%") do if "%%a"=="password" set password=%%b
echo 2. Password - %password%

For /F "UseBackQ tokens=1-2 delims==" %%a in ("%file%") do if "%%a"=="dbname" set dbname=%%b
echo 3. Database - %dbname%

For /F "UseBackQ tokens=1-2 delims==" %%a in ("%file%") do if "%%a"=="postgres" set postgres=%%b
echo 4. Postgres %postgres%

choice.exe /m "Dump database %dbname%?"
if %errorlevel% equ 1 (
    SET PGPASSWORD=%password%

    "%postgres%\bin\pg_dump.exe" -h localhost -p 5432 -U %user% -Fc -d %dbname% --no-owner > "%dbname%.backup"
) else (
    echo Cancel create dump %dbname%
)

pause