@echo off

SetLocal EnableExtensions EnableDelayEdExpansion

set file=config.yml

echo Configuration:

For /F "UseBackQ tokens=1-2 delims==" %%a in ("%file%") do if "%%a"=="user" set user=%%b
echo 1. User - %user%

For /F "UseBackQ tokens=1-2 delims==" %%a in ("%file%") do if "%%a"=="password" set password=%%b
echo 2. Password - %password%

For /F "UseBackQ tokens=1-2 delims==" %%a in ("%file%") do if "%%a"=="dbname" set dbname=%%b
echo 3. Database - %dbname%

For /F "UseBackQ tokens=1-2 delims==" %%a in ("%file%") do if "%%a"=="backup" set backup=%%b
echo 4. Backup - %backup%

For /F "UseBackQ tokens=1-2 delims==" %%a in ("%file%") do if "%%a"=="postgres" set postgres=%%b
echo 5. Postgres %postgres%

set PGPASSWORD=%password%

choice.exe /m "Create database %dbname%?"
if %errorlevel% equ 1 (
    echo Drop database %dbname%
	"%postgres%\bin\dropdb.exe" -h localhost -U %user% --if-exists %dbname%

    echo Create database %dbname%
    "%postgres%\bin\createdb.exe" -h localhost -U %user% %dbname%

    echo Restoring from the backup
    "%postgres%\bin\pg_restore.exe" -h localhost -U %user% --no-owner -d %dbname% "%backup%"
) else (
    echo Cancel create database %dbname%
)

pause
