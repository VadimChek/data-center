#!/usr/bin/env bash

if [[ ! -f .env ]]; then
  echo 'File .env doesn`t exists' >&2
  exit 1
fi

source .env
ACTION=$1

if ! [[ -x "$(command -v docker)" ]]; then
  echo 'Error: Docker not installed. Please install docker from official site, not from your OS repo!' >&2
  exit 1
fi

if ! [[ -x "$(command -v docker-compose)" ]]; then
  echo 'Error: docker-compose not installed. Please install docker-compose from official site, not from your OS repo!' >&2
  exit 1
fi

down() {
  docker-compose -f docker-compose.yml down --remove-orphans
}

up() {
  docker-compose -f docker-compose.yml up --detach
}

initDb() {
  read -p "Хотите перенакатить базу данных ${DB_DATABASE}? y/n " -n 1 -r

  echo

  if [[ $REPLY =~ ^[Yy]$ ]]; then
    dropDb
    createDb
    sourceDb
  fi
}

dropDb() {
  docker exec -i data-center-postgres psql -U postgres <<<"DROP DATABASE IF EXISTS ${DB_DATABASE};"
}

createDb() {
  docker exec -i data-center-postgres psql -U postgres <<<"CREATE DATABASE ${DB_DATABASE};"
}

sourceDb() {
  docker exec -i data-center-postgres bash <<<"pg_restore --username ${DB_USER} --no-owner -d ${DB_DATABASE} /backup/base.dump"
}

initPhp() {
  case "${APP_ENV}" in
  dev) env=Development ;;
  prod) env=Production ;;
  esac

  docker exec \
    -it \
    data-center-php php init --env=$env --overwrite=All
}

install() {
  docker exec \
    -it \
    -e COMPOSER_CACHE_DIR=/tmp/cache \
    data-center-php composer install
}

update() {
  docker exec \
    -it \
    -e COMPOSER_CACHE_DIR=/tmp/cache \
    data-center-php composer update
}

migrate() {
  docker exec \
    -it \
    data-center-php php yii migrate --migrationPath=@yii/rbac/migrations/ --interactive=0

  docker exec \
    -it \
    data-center-php php yii migrate --interactive=0
}

if [[ -z ${ACTION} ]]; then
  down
  up
  initDb
  install
  initPhp
  migrate
fi

case ${ACTION} in
down)
  down
  exit
  ;;
up)
  up
  exit
  ;;
install)
  install
  exit
  ;;
update)
  update
  exit
  ;;
init)
  initPhp
  exit
  ;;
migrate)
  migrate
  exit
  ;;
esac
