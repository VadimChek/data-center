<?php

use common\models\subdivision\Subdivision;
use frontend\forms\subdivision\SubdivisionForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/**
 * @var $this View
 * @var $modelForm SubdivisionForm
 * @var $form ActiveForm
 */
?>

<div class="bid-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($modelForm, 'name')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelForm, 'status')->dropDownList(Subdivision::getStatusLabels()) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php $form::end(); ?>
</div>
