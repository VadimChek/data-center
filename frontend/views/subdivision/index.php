<?php

use common\models\subdivision\Subdivision;
use frontend\models\subdivision\SubdivisionSearch;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\web\View;

/**
 * @var $dataProvider ActiveDataProvider
 * @var $this View
 * @var $searchModel SubdivisionSearch
 * @var $showFilter boolean
 */

$this->title = 'Список подразделений';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'label' => 'ID',
        'value' => function (Subdivision $model) {
            return $model->id;
        }
    ],
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function (Subdivision $model) {
            return $model->name;
        }
    ],
    [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function (Subdivision $model) {
            switch ($model->status) {
                case Subdivision::STATUS_ACTIVE:
                    return '<span class="label label-primary">' . $model->getStatusLabel() . '</span>';
                case Subdivision::STATUS_ARCHIVE:
                    return '<span class="label label-danger">' . $model->getStatusLabel() . '</span>';
                default:
                    return $model->getStatusLabel();
            }
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'header' => 'Действия',
        'headerOptions' => [
            'width' => '100',
        ],
        'template' => '{update} {archive}',
        'buttons' => [
            'update' => function ($url) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'title' => 'Обновить',
                    'class' => 'btn btn-xs btn-primary'
                ]);
            },
            'archive' => function ($url, Subdivision $model) {
                if ($model->isActive()) {
                    return Html::a('<i class="fa fa-archive"></i>', $url, [
                        'title' => 'Архивировать',
                        'class' => 'btn btn-xs btn-danger',
                        'data-confirm' => 'Вы действительно хотите архивировать подразделение?'
                    ]);
                }

                if ($model->isArchive()) {
                    return Html::a('<i class="fa fa-check"></i>', ['/subdivision/restore', 'id' => $model['id']], [
                        'title' => 'Восстановить',
                        'class' => 'btn btn-xs btn-warning',
                        'data-confirm' => 'Вы действительно хотите восстановить данное подразделение?'
                    ]);
                }
            },
        ],
    ],
];
?>

<h1 class="t-a-c m-t-0">
    <?= h($this->title) ?>
</h1>

<?= $this->render('_search', [
    'model' => $searchModel,
    'showFilter' => $showFilter,
]); ?>

<?= DynaGrid::widget([
    'columns' => $columns,
    'theme' => 'simple-default',
    'storage' => DynaGrid::TYPE_COOKIE,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => '<h3 class="panel-title">' . $this->title . '</h3>',
            'before' => '<div class="dynagrid-buttons">' . '{dynagrid}' .
                Html::a('Добавить подразделение', ['create'], ['class' => 'btn btn-default m-l-5 m-r-5']) . '</div>'
        ],
    ],
    'options' => [
        'id' => 'dynagrid-subdivision'
    ]
]); ?>
