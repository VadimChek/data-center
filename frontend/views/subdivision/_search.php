<?php

use frontend\helpers\FilterHelper;
use common\models\subdivision\Subdivision;
use frontend\models\subdivision\SubdivisionSearch;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/**
 * @var $this View
 * @var $model SubdivisionSearch
 * @var $form ActiveForm
 * @var $showFilter boolean
 */
?>

<div class="panel panel-default">
    <div class="panel-heading search-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        <h4 class="panel-title">
            <a class="accordion-toggle <?= $showFilter ? '' : 'collapsed' ?>" data-toggle="collapse"
               data-parent="#accordion" href="#collapseThree">Фильтр</a>
        </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse <?= $showFilter ? 'in' : '' ?>"
         data-filter-type="<?= FilterHelper::TYPE_SUBDIVISION_FILTER ?>">
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'name') ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'status')->dropDownList(Subdivision::getStatusLabels(), [
                        'prompt' => '---'
                    ]) ?>
                </div>
                <div class="col-sm-3"></div>
                <div class="col-sm-3">
                    <div class="form-group t-a-r m-t-25">
                        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Сбросить', ['/subdivision/clear-filter'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
