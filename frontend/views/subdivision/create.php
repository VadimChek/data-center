<?php

use frontend\forms\subdivision\SubdivisionForm;
use yii\web\View;

/**
 * @var $this View
 * @var $modelForm SubdivisionForm
 */

$this->title = 'Добавление подразделения';
$this->params['breadcrumbs'][] = ['label' => 'Подразделения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bid-create">
    <h1 class="t-a-c m-t-0">
        <?= h($this->title) ?>
    </h1>

    <?= $this->render('_form', [
        'modelForm' => $modelForm,
    ]) ?>
</div>
