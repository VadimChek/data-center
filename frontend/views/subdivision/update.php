<?php

use common\helpers\TextHelper;
use common\models\subdivision\Subdivision;
use frontend\forms\subdivision\SubdivisionForm;
use yii\web\View;

/**
 * @var $this View
 * @var $modelForm SubdivisionForm
 * @var $model Subdivision
 */

$this->title = 'Редактирование подразделения: ' . TextHelper::getShortText($model->name, 75);
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="bid-update">
    <h1 class="m-t-0">
        <?= h($this->title) ?>
    </h1>

    <?= $this->render('_form', [
        'modelForm' => $modelForm,
    ]) ?>
</div>
