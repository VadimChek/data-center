<?php

use common\models\event\Events;
use frontend\forms\events\EventsForm;
use kartik\select2\Select2;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var EventsForm $model
 */
?>

<?php $form = ActiveForm::begin([
    'action' => ['/events/create'],
    'method' => 'POST',
    'id' => 'js-create-event-form'
]); ?>

<div class="modal-body">
    <?= $form->field($model, 'name')->textarea([
        'class' => 'form-control resize-both'
    ])->label('Событие') ?>

    <?= $form->field($model, 'type')->widget(Select2::class, [
        'data' => Events::getTypeLabels(),
        'pluginOptions' => [
            'allowClear' => true,
        ],
        'options' => [
            'placeholder' => '---',
            'multiple' => false
        ],
    ]) ?>

    <?= $form->field($model, 'date')->hiddenInput()->label(false)->error(false) ?>

    <div class="text-right">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary js-send-review">Сохранить</button>
    </div>
</div>

<?php $form::end(); ?>
