<?php

use common\helpers\TextHelper;
use common\models\bid\Bid;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\View;

/**
 * @var $this View
 * @var $model Bid
 */

$this->title = TextHelper::getShortText($model->content, 75);
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user->identity;
?>

<div class="bid-view">
    <h1 class="m-t-0">
        <?= h($this->title) ?>
    </h1>
    <p>
        <?php if ($model->isActive()) { ?>
            <?= Html::a('<i class="fa fa-check"></i> Отметить как выполненную', ['check', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => 'Отметить заявку как выполненную?',
                ],
            ]) ?>

            <?= Html::a('<i class="fa fa-hammer"></i> Взять в работу', ['work', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => 'Отметить заявку как принятую в работу?',
                ],
            ]) ?>
        <?php } ?>

        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
            'class' => 'btn btn-info',
            'title' => 'Редактировать',
        ]) ?>

        <?php if ($user->isAdmin() && !$model->isArchive()) { ?>
            <?= Html::a('<i class="fa fa-archive"></i>', ['archive', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'title' => 'Архивировать',
                'data' => [
                    'confirm' => 'Вы уверены что хотите архивировать данную заявку?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php } ?>
    </p>

    <?php if ($model->statusHistory) { ?>
        <div class="panel panel-default">
            <div class="panel-heading search-heading" data-toggle="collapse" data-parent="#accordion"
                 href="#collapseThree">
                <h4 class="panel-title">
                    <a class="accordion-toggle"
                       data-toggle="collapse"
                       data-parent="#accordion"
                       href="#collapseThree">История статусов</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="w25">Изменен пользователем</th>
                        <th class="w25">С</th>
                        <th class="w25">На</th>
                        <th class="w25">Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($model->statusHistory as $statusHistory) { ?>
                        <tr>
                            <td><?= $statusHistory->user->username ?></td>
                            <td><?= $statusHistory->status_from ?></td>
                            <td><?= $statusHistory->status_to ?></td>
                            <td><?= date('d.m.Y H:i:s', strtotime($statusHistory->created_at)) ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered detail-view col-width-50'
        ],
        'attributes' => [
            'number',
            'accepted_user',
            'applicant_user',
            [
                'attribute' => 'department_id',
                'value' => function (Bid $model) {
                    return $model->department ? $model->department->name : '---';
                }
            ],
            'content:ntext',
            'note:ntext',
            'building',
            'floor',
            'room:ntext',
            'phone',
            [
                'attribute' => 'subdivision_id',
                'value' => function (Bid $model) {
                    return $model->subdivision ? $model->subdivision->name : '---';
                }
            ],
            [
                'attribute' => 'status',
                'value' => function (Bid $model) {
                    return $model->getStatusLabel();
                }
            ],
            [
                'attribute' => 'date',
                'value' => function (Bid $model) {
                    return date('Y-m-d H:i', strtotime($model->date));
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function (Bid $model) {
                    return date('Y-m-d H:i', strtotime($model->updated_at));
                }
            ],
        ],
    ]) ?>
</div>
