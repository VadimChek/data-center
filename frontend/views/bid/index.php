<?php

use common\models\bid\Bid;
use frontend\models\bid\BidSearch;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\data\ActiveDataProvider;

/**
 * @var $this View
 * @var $searchModel BidSearch
 * @var $dataProvider ActiveDataProvider
 * @var $showFilter boolean
 */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user->identity;
$isAdmin = $this->params['global-is-admin'] ?? false;

$columns = [
    [
        'class' => 'yii\grid\CheckboxColumn',
    ],
    [
        'attribute' => 'number',
        'label' => '№',
    ],
    [
        'attribute' => 'department_id',
        'value' => function (Bid $model) {
            return $model->department->name;
        }
    ],
    [
        'attribute' => 'accepted_user',
        'label' => 'Принял заявку',
    ],
    [
        'attribute' => 'applicant_user',
        'contentOptions' => ['style' => 'white-space: pre-wrap'],
    ],
    [
        'attribute' => 'date',
        'format' => 'raw',
        'value' => function (Bid $model) {
            return
                date('H:i', strtotime($model->date)) . '<br>' .
                date('d-m-Y', strtotime($model->date));
        }
    ],
    [
        'attribute' => 'content',
        'format' => 'raw',
        'contentOptions' => ['style' => 'white-space: pre-wrap'],
        'value' => function (Bid $model) {
            $content = h($model->content);
            if ($model->note) {
                $content .= '<div class="divider"></div>';
                $content .= h($model->note);
            }

            return $content;
        }
    ],
    [
        'attribute' => 'building',
        'label' => 'Место',
        'format' => 'raw',
        'contentOptions' => ['style' => 'white-space: pre-wrap'],
        'value' => function (Bid $model) {
            $marker = $model->subdivision
                ? Html::tag('i', null, [
                    'class' => 'fas fa-map-marker-alt text-color-light pointer',
                    'title' => h($model->subdivision->name),
                    'data' => [
                        'toggle' => 'tooltip',
                        'placement' => 'top',
                    ],
                ])
                : '';

            $view = '<a href="' . Url::to(['/bid/view', 'id' => $model->id]) . '">' . Html::tag('i', null, [
                    'class' => 'fas fa-eye',
                    'title' => 'Просмотр заявки',
                    'data' => [
                        'toggle' => 'tooltip',
                        'placement' => 'top',
                    ],
                ]) . '</a>';

            $phone = $model->phone
                ? '<i class="fa fa-phone"></i> <b>' . $model->phone
                : '';

            $building = 'Корпус:&nbsp;' . ($model->building ? $model->building : '---');
            $floor = 'Этаж:&nbsp;' . ($model->floor ? $model->floor : '---');
            $room = 'Помещение: ' . ($model->room ? $model->room : '---');

            return $phone . '&nbsp;' . $marker . '&nbsp;' . $view . '</b><br>' . $building . '. ' . $floor . '.' . '<br>' . $room;
        }
    ],
    [
        'attribute' => 'status',
        'label' => 'Статус',
        'format' => 'raw',
        'filter' => Bid::getStatusLabels(),
        'value' => function (Bid $model) {
            switch ($model->status) {
                case Bid::STATUS_ACTIVE:
                    return '<span class="label label-primary">' . $model->getStatusLabel() . '</span>';
                case Bid::STATUS_IN_WORK:
                    return '<span class="label label-warning">' . $model->getStatusLabel() . '</span>';
                case Bid::STATUS_COMPLETE:
                    return '<span class="label label-success">' . $model->getStatusLabel() . '</span>';
                case Bid::STATUS_NOT_COMPLETE:
                case Bid::STATUS_ARCHIVE:
                    return '<span class="label label-danger">' . $model->getStatusLabel() . '</span>';
                default:
                    return $model->getStatusLabel();
            }
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{work} {check} {update} {archive}',
        'buttons' => [
            'work' => function ($url, Bid $model) {
                if ($model->isActive()) {
                    return Html::a('<i class="fa fa-hammer"></i>', ['/bid/work', 'id' => $model['id']], [
                            'title' => 'Отметить как принятую в работу',
                            'class' => 'btn btn-xs btn-primary m-b-5',
                            'data-confirm' => 'Отметить заявку как принятую в работу?',
                        ]) . '<br>';
                }
                return '';
            },
            'check' => function ($url, Bid $model) {
                if ($model->isActive() || $model->isInWork()) {
                    return Html::a('<i class="fa fa-check"></i>', ['/bid/check', 'id' => $model['id']], [
                            'title' => 'Отметить как выполненное',
                            'class' => 'btn btn-xs btn-success m-b-5',
                            'data-confirm' => 'Отметить заявку как выполненную?',
                        ]) . '<br>';
                }
                return '';
            },
            'update' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/bid/update', 'id' => $model['id']], [
                    'title' => 'Обновить',
                    'class' => 'btn btn-xs btn-warning'
                ]);
            },
            'archive' => function ($url, Bid $model) use ($user) {
                if ($model->isArchive()) {
                    return '';
                }

                if ($user->isAdmin()) {
                    return Html::a('<i class="fa fa-archive"></i>', ['/bid/archive', 'id' => $model['id']], [
                        'title' => 'Архивировать',
                        'class' => 'btn btn-xs btn-danger',
                        'data-confirm' => 'Вы действительно хотите архивировать заявку?'
                    ]);
                }
                return '';
            },
        ],
    ],
];

$archivePackLink = $isAdmin
    ? Html::button('Скрыть', ['class' => 'btn btn-primary m-l-5 m-r-5 js-archive-bid'])
    : '';
?>

<h1 class="t-a-c m-t-0">
    <?= h($this->title) ?>
</h1>

<?= $this->render('_search', [
    'model' => $searchModel,
    'showFilter' => $showFilter,
]); ?>

<?= DynaGrid::widget([
    'columns' => $columns,
    'theme' => 'simple-default',
    'storage' => DynaGrid::TYPE_COOKIE,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => '<h3 class="panel-title">' . $this->title . '</h3>',
            'before' => '<div class="dynagrid-buttons">' . '{dynagrid}' .
                Html::a('Добавить заявку', ['create'], ['class' => 'btn btn-success m-l-5 m-r-5']) .
                $archivePackLink . '</div>'
        ],
        'rowOptions' => function (Bid $model) {
            if ($model->isImportant()) {
                return [
                    'class' => 'color-light-red'
                ];
            }
        },
    ],
    'options' => [
        'id' => 'dynagrid-bid'
    ]
]); ?>
