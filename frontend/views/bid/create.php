<?php

use frontend\forms\bid\BidForm;
use yii\web\View;

/**
 * @var $this View
 * @var $modelForm BidForm
 * @var $lastAcceptedUser string
 */

$this->title = 'Создание заявки';
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bid-create">
    <h1 class="t-a-c m-t-0">
        <?= h($this->title) ?>
    </h1>
    <?= $this->render('_form', compact('modelForm', 'lastAcceptedUser')) ?>
</div>
