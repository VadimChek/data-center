<?php

use frontend\forms\bid\BidForm;
use common\models\bid\Bid;
use common\helpers\TextHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $model Bid
 * @var $modelForm BidForm
 * @var $lastAcceptedUser string
 */

$this->title = 'Редактирование заявки: ' . TextHelper::getShortText($model->content, 75);
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => TextHelper::getShortText($model->content, 75), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="bid-update">
    <h1 class="m-t-0">
        <?= h($this->title) ?>
    </h1>

    <?= $this->render('_form', [
        'modelForm' => $modelForm,
        'lastAcceptedUser' => $lastAcceptedUser,
    ]) ?>
</div>
