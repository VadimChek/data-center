<?php

use frontend\forms\bid\BidForm;
use common\models\bid\Bid;
use common\models\department\Department;
use common\models\subdivision\Subdivision;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\widgets\MaskedInput;

/**
 * @var $this View
 * @var $modelForm BidForm
 * @var $form ActiveForm
 * @var $lastAcceptedUser string
 */

$isAdmin = $this->params['global-is-admin'] ?? false;
?>

<div class="bid-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($modelForm, 'number')->textInput([
                'disabled' => true,
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelForm, 'accepted_user')->textInput([
                'class' => 'form-control js-input-content',
                'maxlength' => true,
            ])->label($modelForm->getAttributeLabel('accepted_user') . '&nbsp;' . '<span class="label-extend js-label-content">' . $lastAcceptedUser . '</span>') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($modelForm, 'applicant_user')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelForm, 'department_id')->widget(Select2::class, [
                'data' => Department::getAllList([Department::ITC]),
                'pluginOptions' => [
                    'allowClear' => true,
                ],
                'options' => [
                    'placeholder' => '-----',
                    'multiple' => false
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($modelForm, 'building')->input('number') ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($modelForm, 'floor')->dropDownList(Bid::getFloors(), ['prompt' => '---']) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($modelForm, 'room')->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($modelForm, 'phone')
                ->textInput(['placeholder' => '99-99'])
                ->widget(MaskedInput::class, [
                    'mask' => '99-99',
                ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($modelForm, 'subdivision_id')->widget(Select2::class, [
                'data' => Subdivision::getListForSelect(),
                'pluginOptions' => [
                    'allowClear' => true,
                ],
                'options' => [
                    'placeholder' => '-----',
                    'multiple' => false
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($modelForm, 'content')->textarea([
                'rows' => 3,
                'class' => 'form-control resize-both',
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelForm, 'note')->textarea([
                'rows' => 3,
                'class' => 'form-control resize-both',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php if ($isAdmin) { ?>
                <?= $form->field($modelForm, 'status')->dropDownList(Bid::getRbacStatusLabelsForAdmin()) ?>
            <?php } else { ?>
                <?= $form->field($modelForm, 'status')->dropDownList(Bid::getRbacStatusLabels()) ?>
            <?php } ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelForm, 'date')
                ->textInput(['placeholder' => '9999-99-99 99:99'])
                ->widget(MaskedInput::class, [
                    'mask' => '9999-99-99 99:99',
                ]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php $form::end(); ?>
</div>
