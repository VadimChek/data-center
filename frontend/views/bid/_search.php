<?php

use frontend\helpers\FilterHelper;
use common\models\bid\Bid;
use frontend\models\bid\BidSearch;
use common\models\department\Department;
use common\models\subdivision\Subdivision;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\widgets\MaskedInput;

/**
 * @var $this View
 * @var $model BidSearch
 * @var $form ActiveForm
 * @var $showFilter boolean
 */
?>

<div class="panel panel-default">
    <div class="panel-heading search-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        <h4 class="panel-title">
            <a class="accordion-toggle <?= $showFilter ? '' : 'collapsed' ?>" data-toggle="collapse"
               data-parent="#accordion" href="#collapseThree">Фильтр</a>
        </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse <?= $showFilter ? 'in' : '' ?>"
         data-filter-type="<?= FilterHelper::TYPE_BID_FILTER ?>">
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'departmentId')->widget(Select2::class, [
                        'data' => Department::getAllList([Department::ITC]),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => '-----',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'subdivisionId')->widget(Select2::class, [
                        'data' => Subdivision::getListForSelect(),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => '-----',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'acceptedUser') ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'applicantUser') ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'number') ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'phone')
                        ->textInput(['placeholder' => '99-99'])
                        ->widget(MaskedInput::class, [
                            'mask' => '99-99',
                        ]) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'building')->textInput() ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'floor')->widget(Select2::class, [
                        'data' => Bid::getFloors(),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => '-----',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'room') ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'status')->widget(Select2::class, [
                        'data' => Bid::getFilterStatusLabels(),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => '-----',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'dateFrom')->widget(DatePicker::class, [
                        'options' => [
                            'placeholder' => '',
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'dateTo')->widget(DatePicker::class, [
                        'options' => [
                            'placeholder' => '',
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'isVisible')->widget(Select2::class, [
                        'data' => Bid::getVisibleLabels(),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => '-----',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-2">
                    <div class="form-group t-a-r m-t-25">
                        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Сбросить', ['/bid/clear-filter'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>