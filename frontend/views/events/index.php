<?php

use frontend\models\event\EventsSearch;
use frontend\models\statistics\DateForm;
use yii\web\View;

/**
 * @var $this View
 * @var $dateForm DateForm
 * @var $searchModel EventsSearch
 * @var $period DatePeriod
 * @var $events array
 * @var $currentDay string
 * @var $showFilter boolean
 */

$this->title = 'События';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="calendar-index">
    <h1 class="t-a-c m-t-0">
        <?= h($this->title) ?>
    </h1>

    <?= $this->render('_search', [
        'model' => $dateForm,
        'searchModel' => $searchModel,
        'showFilter' => $showFilter,
    ]); ?>

    <?= $this->render('@frontend/views/inc/calendar', [
        'events' => $events,
        'period' => $period,
        'currentDay' => $currentDay,
    ]) ?>
</div>
