<?php

use common\models\event\Events;
use frontend\helpers\FilterHelper;
use frontend\models\event\EventsSearch;
use frontend\models\statistics\DateForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/**
 * @var $this View
 * @var $model DateForm
 * @var $searchModel EventsSearch
 * @var $form ActiveForm
 * @var $showFilter boolean
 */
?>

<div class="panel panel-default">
    <div class="panel-heading search-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        <h4 class="panel-title">
            <a class="accordion-toggle <?= $showFilter ? '' : 'collapsed' ?>" data-toggle="collapse"
               data-parent="#accordion" href="#collapseThree">Фильтр</a>
        </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse <?= $showFilter ? 'in' : '' ?>"
         data-filter-type="<?= FilterHelper::TYPE_EVENTS_FILTER ?>">
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'dateFrom')->widget(DatePicker::class, [
                        'options' => [
                            'placeholder' => '',
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'dateTo')->widget(DatePicker::class, [
                        'options' => [
                            'placeholder' => '',
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($searchModel, 'type')->widget(Select2::class, [
                        'data' => Events::getTypeLabels(),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => '-----',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <div class="form-group t-a-r m-t-25">
                        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Сбросить', ['/events/clear-filter'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
