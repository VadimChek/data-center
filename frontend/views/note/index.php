<?php

use frontend\assets\BootstrapWysiwygAsset;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $content string
 */

$this->title = 'Заметки (общие)';
$this->params['breadcrumbs'][] = $this->title;

BootstrapWysiwygAsset::register($this)
?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="t-a-c m-t-0">
                <?= h($this->title) ?>
            </h2>
            <hr>
            <div class="btns-wrapper">
                <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Размер текста">
                            <i class="fa fa-text-height"></i>&nbsp;<b class="fa fa-caret-down"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a data-edit="fontSize 5"><span style="font-size: large; ">Большой</span></a></li>
                            <li><a data-edit="fontSize 3"><span style="font-size: small; ">Нормальный</span></a></li>
                            <li><a data-edit="fontSize 1"><span style="font-size: xx-small; ">Маленький</span></a></li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="bold" title="Жирный (Ctrl+B)">
                            <i class="fa fa-bold"></i>
                        </a>
                        <a class="btn" data-edit="italic" title="Курсив (Ctrl+I)">
                            <i class="fa fa-italic"></i>
                        </a>
                        <a class="btn" data-edit="strikethrough" title="Зачеркнутый">
                            <i class="fa fa-strikethrough"></i>
                        </a>
                        <a class="btn" data-edit="underline" title="Подчеркивание (Ctrl+U)">
                            <i class="fa fa-underline"></i>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="insertunorderedlist" title="Маркированный список">
                            <i class="fa fa-list-ul"></i>
                        </a>
                        <a class="btn" data-edit="insertorderedlist" title="Нумерованный список">
                            <i class="fa fa-list-ol"></i>
                        </a>
                        <a class="btn" data-edit="outdent" title="Отступ влево">
                            <i class="fa fa-caret-square-left"></i>
                        </a>
                        <a class="btn" data-edit="indent" title="Отступ вправо">
                            <i class="fa fa-caret-square-right"></i>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="justifyleft" title="Слева (Ctrl+L)">
                            <i class="fa fa-align-left"></i>
                        </a>
                        <a class="btn" data-edit="justifycenter" title="По центру (Ctrl+E)">
                            <i class="fa fa-align-center"></i>
                        </a>
                        <a class="btn" data-edit="justifyright" title="Справа (Ctrl+R)">
                            <i class="fa fa-align-right"></i>
                        </a>
                        <a class="btn" data-edit="justifyfull" title="По ширине (Ctrl+J)">
                            <i class="fa fa-align-justify"></i>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="undo" title="Отменить (Ctrl+Z)">
                            <i class="fa fa-undo"></i>
                        </a>
                        <a class="btn" data-edit="redo" title="Повторить (Ctrl+Y)">
                            <i class="fa fa-redo"></i>
                        </a>
                    </div>
                </div>
                <div>
                    <?= Html::button('Сохранить', [
                        'id' => 'js-note-save',
                        'class' => 'btn btn-success'
                    ]) ?>

                    <?= Html::a('Отметить как прочитанное', ['/note/read'], [
                        'class' => 'btn btn-primary',
                        'data-confirm' => 'Отметить как прочитанное?'
                    ]) ?>
                </div>
            </div>
            <div id="editor" class="m-t-10">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>