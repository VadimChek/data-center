<?php

use common\models\rbac\AuthItem;
use common\models\user\User;
use frontend\forms\profile\ProfileForm;
use common\models\department\Department;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\MaskedInput;

/**
 * @var View $this
 * @var User $model
 * @var ProfileForm $modelForm
 */

$this->title = 'Редактирование пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Список пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => h($model->username), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="t-a-c m-t-0">
                <?= h($this->title) ?>
            </h1>
            <hr>
            <?php $form = ActiveForm::begin(['id' => 'form-profile']); ?>
            <div class="row">
                <div class="col-sm-4">
                    <?= $form->field($modelForm, 'firstname')->textInput([
                        'placeholder' => 'Имя'
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($modelForm, 'secondname')->textInput([
                        'placeholder' => 'Фимилия'
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($modelForm, 'lastname')->textInput([
                        'placeholder' => 'Отчество'
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?= $form->field($modelForm, 'username')->textInput([
                        'placeholder' => 'Логин'
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($modelForm, 'department_id')->widget(Select2::class, [
                        'data' => Department::getAllList([Department::ITC]),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => 'Выберите отдел',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($modelForm, 'phone')
                        ->textInput(['placeholder' => '+9 (999) 999 9999'])
                        ->widget(MaskedInput::class, [
                            'mask' => '+9 (999) 999 9999',
                            'clientOptions' => [
                                'removeMaskOnSubmit' => true,
                            ],
                        ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?= $form->field($modelForm, 'role')->widget(Select2::class, [
                        'data' => AuthItem::getAllRoles(),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => 'Выберите роль',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($modelForm, 'newPassword')->textInput([
                        'placeholder' => 'Новый пароль',
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($modelForm, 'rNewPassword')->textInput([
                        'placeholder' => 'Повторите новый пароль'
                    ])->label('Повторите новый пароль') ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php $form::end(); ?>
        </div>
    </div>
</div>