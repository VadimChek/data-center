<?php

use common\models\user\User;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\View;

/**
 * @var $this View
 * @var $model User
 */

$this->title = h($model->getFio());
$this->params['breadcrumbs'][] = ['label' => 'Список пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-view">
    <h1>
        <?= h($this->title) ?>
    </h1>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить пользователя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered',
        ],
        'attributes' => [
            'id',
            'username',
            [
                'attribute' => 'firstname',
                'label' => 'ФИО',
                'value' => function (User $model) {
                    return $model->getFio();
                }
            ],
            [
                'attribute' => 'status',
                'value' => function (User $model) {
                    return $model->getStatusLabel();
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function (User $model) {
                    return date('Y-m-d H:i:s', strtotime($model->created_at));
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function (User $model) {
                    return date('Y-m-d H:i:s', strtotime($model->updated_at));
                }
            ],
        ],
    ]) ?>
</div>