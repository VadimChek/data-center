<?php

use common\models\rbac\AuthItem;
use common\models\user\User;
use common\models\department\Department;
use frontend\models\user\UserSearch;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\web\View;

/**
 * @var $dataProvider ActiveDataProvider
 * @var $this View
 * @var $searchModel UserSearch
 * @var $showFilter boolean
 */

$this->title = 'Список пользователей';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'label' => 'ID',
        'value' => function (User $model) {
            return $model->id;
        }
    ],
    [
        'attribute' => 'fio',
        'label' => 'ФИО',
        'format' => 'raw',
        'value' => function (User $model) {
            if (Yii::$app->authManager->checkAccess($model->id, AuthItem::ROLE_ADMIN)) {
                $userIcon = '<i class="fas fa-user-secret" title="Администратор"></i>';
            } elseif (Yii::$app->authManager->checkAccess($model->id, AuthItem::ROLE_OPERATOR)) {
                $userIcon = '<i class="fas fa-user" title="Оператор"></i>';
            } else {
                $userIcon = '<i class="fas fa-user-tie" title="Начальство"></i>';
            }

            return $userIcon . '&nbsp;' . h($model->getFio());
        }
    ],
    [
        'attribute' => 'department_id',
        'label' => 'Отдел',
        'filter' => Department::getAllList(),
        'value' => function (User $model) {
            return $model->department ? $model->department->name : '---';
        }
    ],
    [
        'attribute' => 'status',
        'label' => 'Статус',
        'filter' => User::getStatusLabels(),
        'value' => function (User $model) {
            return User::getStatusLabels()[$model->status];
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'header' => 'Действия',
        'headerOptions' => [
            'width' => '100',
        ],
        'template' => '{view} {update} {delete}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/admin/user/view', 'id' => $model['id']], [
                    'title' => 'Просмотр',
                    'class' => 'btn btn-xs btn-info'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/admin/user/update', 'id' => $model['id']], [
                    'title' => 'Обновить',
                    'class' => 'btn btn-xs btn-primary'
                ]);
            },
            'restore' => function ($url, $model) {
                return Html::a('<span class="fa fa-trash-restore"></span>', ['/admin/user/restore', 'id' => $model['id']], [
                    'title' => 'Восстановить',
                    'class' => 'btn btn-xs btn-success'
                ]);
            },
            'delete' => function ($url, User $model) {
                if (!$model->isDeleted()) {
                    return Html::a('<i class="fa fa-trash"></i>', ['/admin/user/delete', 'id' => $model['id']], [
                        'title' => 'Удалить',
                        'class' => 'btn btn-xs btn-danger',
                        'data-confirm' => 'Вы действительно хотите удалить данного пользователя?'
                    ]);
                }

                if ($model->isDeleted()) {
                    return Html::a('<i class="fa fa-check"></i>', ['/admin/user/restore', 'id' => $model['id']], [
                        'title' => 'Восстановить',
                        'class' => 'btn btn-xs btn-warning',
                        'data-confirm' => 'Вы действительно хотите восстановить данного пользователя?'
                    ]);
                }
            },
        ],
    ],
];
?>

<h1 class="t-a-c m-t-0">
    <?= h($this->title) ?>
</h1>

<?= $this->render('_search', [
    'model' => $searchModel,
    'showFilter' => $showFilter,
]); ?>

<?= DynaGrid::widget([
    'columns' => $columns,
    'theme' => 'simple-default',
    'storage' => DynaGrid::TYPE_COOKIE,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => '<h3 class="panel-title">' . $this->title . '</h3>',
            'before' => '<div class="dynagrid-buttons">' . '{dynagrid}' .
                Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-default m-l-5 m-r-5']) . '</div>'
        ],
    ],
    'options' => [
        'id' => 'dynagrid-user'
    ]
]); ?>
