<?php

use common\models\user\User;
use common\models\department\Department;
use frontend\helpers\FilterHelper;
use frontend\models\user\UserSearch;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/**
 * @var $this View
 * @var $model UserSearch
 * @var $form ActiveForm
 * @var $showFilter boolean
 */
?>

<div class="panel panel-default">
    <div class="panel-heading search-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        <h4 class="panel-title">
            <a class="accordion-toggle <?= $showFilter ? '' : 'collapsed' ?>" data-toggle="collapse"
               data-parent="#accordion" href="#collapseThree">Фильтр</a>
        </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse <?= $showFilter ? 'in' : '' ?>"
         data-filter-type="<?= FilterHelper::TYPE_USER_FILTER ?>">
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'fio') ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'status')->widget(Select2::class, [
                        'data' => User::getStatusLabels(),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => '-----',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'departmentId')->widget(Select2::class, [
                        'data' => Department::getAllList(),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => '-----',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <div class="form-group t-a-r m-t-25">
                        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Сбросить', ['/admin/user/clear-filter'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>