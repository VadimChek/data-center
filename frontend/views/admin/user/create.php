<?php

/**
 * @var $form ActiveForm
 * @var $this View
 * @var $model SignupForm
 */

use common\models\rbac\AuthItem;
use common\models\department\Department;
use kartik\select2\Select2;
use yii\web\View;
use frontend\forms\SignupForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Создание пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <h2 class="t-a-c m-t-0">
        <?= h($this->title) ?>
    </h2>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'role')->widget(Select2::class, [
                        'data' => AuthItem::getAllRoles(),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => 'Выберите роль',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'department_id')->widget(Select2::class, [
                        'data' => Department::getAllList([Department::ITC]),
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => 'Выберите отдел',
                            'multiple' => false
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'firstname')->textInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'secondname')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'lastname')->textInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'username')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'rPassword')->passwordInput() ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>