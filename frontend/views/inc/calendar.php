<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $currentDay string
 * @var $period DatePeriod
 * @var $events array
 */
?>

<div class="calendar-wrapper">
    <?php foreach ($period as $key => $day) { ?>
        <?php $dayNumber = $day->format('N') ?>
        <?php if ($key == 0) { ?>
            <?php for ($i = 1; $i < $dayNumber; $i++) { ?>
                <div class="day"></div>
            <?php } ?>
        <?php } ?>

        <?php
        $headerColor = '';
        if ($dayNumber >= 6) {
            $headerColor = 'color-light-blue';
        }
        if ($currentDay == $day->format('Y-m-d')) {
            $headerColor = 'color-light-green';
        }
        ?>

        <div class="day border white">
            <div class="header <?= $headerColor ?>">
                <div class="date">
                    <?= Yii::$app->formatter->asDate($day->format('Y-m-d'), 'php: d M Y (D)') . '<br>' ?>
                </div>
                <div class="actions">
                    <i class="fa fa-plus-circle text-success pointer js-create-event"
                       data-date="<?= $day->format('Y-m-d') ?>"></i>
                </div>
            </div>
            <div class="body">
                <?php if ($events[$day->format('Y-m-d')]) { ?>
                    <ol class="ol events">
                        <?php foreach ($events[$day->format('Y-m-d')] as $event) { ?>
                            <li>
                                <?= $event['name'] ?>
                                <?= Html::a('<i class="fa fa-times-circle text-danger delete-event"></i>', ['/events/delete', 'id' => $event['id']], [
                                    'data-confirm' => 'Вы действительно хотите удалить событие?'
                                ]) ?>
                            </li>
                        <?php } ?>
                    </ol>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?php $endNumber = $period->end->format('N');
    if ($endNumber != 7) { ?>
        <?php for ($i = $endNumber; $i < 8; $i++) { ?>
            <div class="day"></div>
        <?php } ?>
    <?php } ?>
</div>
