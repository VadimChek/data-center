<?php

use frontend\models\statistics\DateForm;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\HighchartsAsset;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $dateForm DateForm
 * @var $showFilter boolean
 * @var $countBidStat array
 * @var $buildingStat array
 * @var $departmentStat array
 * @var $subdivisionStat array
 */

HighchartsAsset::register($this);

$this->title = 'Статистика';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 class="t-a-c m-t-0">
    <?= h($this->title) ?>
</h1>

<?= $this->render('_search', [
    'model' => $dateForm,
    'showFilter' => $showFilter,
]); ?>

<div class="container">
    <div class="row m-row">
        <div class="col-sm-12">
            <?= Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Кол-во заявок по подразделениям ВИТ ЭРА'],
                    'credits' => [
                        'enabled' => false,
                    ],
                    'legend' => 'false',
                    'xAxis' => [
                        'type' => 'category',
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество заявок'],
                        'minTickInterval' => 1,
                    ],
                    'tooltip' => [
                        'pointFormat' => '<b>{point.name}</b>: {point.y}'
                    ],
                    'series' => [
                        [
                            'type' => 'bar',
                            'data' => $subdivisionStat,
                            'dataLabels' => [
                                'enabled' => 'true',
                            ],
                        ],
                    ],
                    'showInLegend' => 'false',
                    'dataLabels' => ['enabled' => 'false'],
                ],
            ]); ?>
        </div>
        <hr>
        <div class="col-sm-12">
            <?= Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Кол-во заявок по дням'],
                    'credits' => [
                        'enabled' => false,
                    ],
                    'legend' => 'false',
                    'xAxis' => [
                        'type' => 'category',
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество заявок'],
                        'minTickInterval' => 1,
                    ],
                    'tooltip' => [
                        'pointFormat' => 'Количество заявок <b>{point.y} шт.</b>'
                    ],
                    'series' => [
                        [
                            'type' => 'line',
                            'data' => $countBidStat,
                            'dataLabels' => [
                                'enabled' => 'true',
                            ],
                        ],
                    ],
                    'showInLegend' => 'false',
                    'dataLabels' => ['enabled' => 'false'],
                ],
            ]); ?>
        </div>
        <hr>
        <div class="col-sm-6">
            <?= Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Кол-во заявок по отделам'],
                    'credits' => [
                        'enabled' => false,
                    ],
                    'legend' => 'false',
                    'xAxis' => [
                        'type' => 'category',
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество заявок'],
                        'minTickInterval' => 1,
                    ],
                    'tooltip' => [
                        'pointFormat' => 'Количество заявок <b>{point.y} шт.</b>'
                    ],
                    'series' => [
                        [
                            'type' => 'pie',
                            'data' => $departmentStat,
                            'dataLabels' => [
                                'enabled' => 'true',
                            ],
                        ],
                    ],
                    'showInLegend' => 'false',
                    'dataLabels' => ['enabled' => 'false'],
                ],
            ]); ?>
        </div>
    </div>
</div>
