<?php

use frontend\forms\profile\ProfileForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\MaskedInput;

/**
 * @var View $this
 * @var ProfileForm $modelForm
 */

$this->title = 'Личные данные';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="t-a-c m-t-0">
                <?= h($this->title) ?>
            </h1>
            <hr>
            <?php $form = ActiveForm::begin(['id' => 'form-profile']); ?>
            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($modelForm, 'firstname')->textInput([
                        'placeholder' => 'Имя'
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($modelForm, 'secondname')->textInput([
                        'placeholder' => 'Фимилия'
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($modelForm, 'lastname')->textInput([
                        'placeholder' => 'Отчество'
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($modelForm, 'username')->textInput([
                        'placeholder' => 'Логин'
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($modelForm, 'phone')
                        ->textInput(['placeholder' => '+9 (999) 999 9999'])
                        ->widget(MaskedInput::class, [
                            'mask' => '+9 (999) 999 9999',
                            'clientOptions' => [
                                'removeMaskOnSubmit' => true,
                            ],
                        ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($modelForm, 'newPassword')->textInput([
                        'placeholder' => 'Новый пароль',
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($modelForm, 'rNewPassword')->textInput([
                        'placeholder' => 'Повторите новый пароль'
                    ])->label('Повторите новый пароль') ?>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success',]) ?>
            </div>
            <?php $form::end(); ?>
        </div>
    </div>
</div>