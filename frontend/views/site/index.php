<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var $currentDay string
 * @var $events array
 * @var $this View
 * @var $period DatePeriod
 */

$this->title = Yii::$app->params['projectName'];
?>

<div class="row m-t-25">
    <div class="col-sm-12">
        <div class="text-right -m-b-10 fs-12">
            <?= Html::a('перейти в календарь', ['/events/index']) ?>
        </div>
        <?= $this->render('@frontend/views/inc/calendar', [
            'events' => $events,
            'period' => $period,
            'currentDay' => $currentDay,
        ]) ?>
    </div>
</div>