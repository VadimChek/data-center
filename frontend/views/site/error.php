<?php

use yii\web\View;

/**
 * @var $this View
 * @var $name string
 * @var $message string
 * @var $exception Exception
 */

$this->title = $name;
?>
<div class="site-error">
    <h1><?= h($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(h($message)) ?>
    </div>
</div>
