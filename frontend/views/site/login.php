<?php

use common\forms\login\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/**
 * @var $this View
 * @var $form ActiveForm
 * @var $model LoginForm
 */

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <h2 class="t-a-c m-t-0">
        <?= h($this->title) ?>
    </h2>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Войти', [
                    'class' => 'btn btn-primary',
                    'name' => 'login-button'
                ]) ?>
            </div>
            <?php $form::end(); ?>
        </div>
        <div class="col-lg-3"></div>
    </div>
</div>