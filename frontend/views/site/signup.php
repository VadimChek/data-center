<?php

use common\models\department\Department;
use yii\web\View;
use frontend\forms\SignupForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this View
 * @var $form ActiveForm
 * @var $model SignupForm
 */

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <h2 class="t-a-c m-t-0">
        <?= h($this->title) ?>
    </h2>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'firstname')->textInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'secondname')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'lastname')->textInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'username')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'department_id')->dropDownList(Department::getAllList([Department::ITC]), [
                        'prompt' => '---'
                    ]) ?>
                </div>
                <div class="col-sm-6"></div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'rPassword')->passwordInput() ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Зарегистрироваться', [
                    'class' => 'btn btn-primary',
                    'name' => 'signup-button',
                ]) ?>
            </div>
            <?php $form::end(); ?>
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>
