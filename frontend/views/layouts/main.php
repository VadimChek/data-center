<?php

use common\widgets\AlertJbox;
use yii\helpers\Html;
use yii\web\View;
use frontend\assets\AppAsset;
use yii\widgets\Breadcrumbs;

/**
 * @var $this View
 * @var $content string
 */

AppAsset::register($this);
$layoutClasses = $this->params['layout-classes'];
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= h($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<div class="fon"></div>
<?php $this->beginBody() ?>

<div class="wrap">
    <?= $this->render('header.php') ?>
    <div class="container m-t-70">
        <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]); ?>
    </div>
    <div class="container <?= $layoutClasses ?>">
        <?= AlertJbox::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= h(Yii::$app->params['projectName']) ?> <?= date('Y') ?></p>
    </div>
</footer>

<?= $this->render('_modals') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
