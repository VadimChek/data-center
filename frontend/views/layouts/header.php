<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $content string
 */

$user = Yii::$app->user->identity;
$isGuest = $this->params['global-is-guest'] ?? false;
$isAdmin = $this->params['global-is-admin'] ?? false;
$bidCount = $this->params['statistics-bid-count'] ?? 0;
$userCount = $this->params['statistics-user-count'] ?? 0;
$todayCountEvents = $this->params['statistics-today-count-events'] ?? 0;
$projectName = Yii::$app->params['projectName'];
?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button"
                    class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#navbar"
                    aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Навигация</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?= Html::a(h($projectName), ['/'], [
                'class' => 'navbar-brand',
            ]) ?>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php if (!$isGuest) { ?>
                    <li>
                        <?= Html::a('Новая заявка', ['/bid/create']) ?>
                    </li>
                    <li>
                        <?php if ($bidCount > 0) { ?>
                            <?= Html::a("Заявки&nbsp;({$bidCount})", ['/bid/index']) ?>
                        <?php } else { ?>
                            <?= Html::a("Заявки", ['/bid/index']) ?>
                        <?php } ?>
                    </li>
                    <li>
                        <?php if ($todayCountEvents > 0) { ?>
                            <?= Html::a("События&nbsp;({$todayCountEvents})", ['/events/index']) ?>
                        <?php } else { ?>
                            <?= Html::a("События", ['/events/index']) ?>
                        <?php } ?>
                    </li>
                    <li>
                        <?= Html::a('Статистика', ['/statistics/index']) ?>
                    </li>
                    <li>
                        <?= Html::a('Справочник', ['/docs/index']) ?>
                    </li>
                    <li>
                        <?php if ($user->profile->isReadNotes()) { ?>
                            <?= Html::a('<i class="far fa-sticky-note"></i>', ['/note'], [
                                'title' => 'Заметки',
                                'data' => [
                                    'toggle' => 'tooltip',
                                    'placement' => 'bottom',
                                ],
                            ]) ?>
                        <?php } else { ?>
                            <?= Html::a('<i class="fas fa-sticky-note text-danger"></i>', ['/note'], [
                                'title' => 'Заметки',
                                'data' => [
                                    'toggle' => 'tooltip',
                                    'placement' => 'bottom',
                                ],
                            ]) ?>
                        <?php } ?>
                    </li>
                <?php } else { ?>
                    <li>
                        <?= Html::a('Справочник', ['/docs/index']) ?>
                    </li>
                <?php } ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if ($isGuest) { ?>
                    <li>
                        <?= Html::a('Авторизация', ['/login']) ?>
                    </li>
                <?php } else { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false"><?= $user->username ?>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <?= Html::a('ЛК', ['/profile']) ?>
                            </li>
                            <?php if ($isAdmin) { ?>
                                <li>
                                    <?php if ($userCount > 0) { ?>
                                        <?= Html::a("Пользователи&nbsp;({$userCount})", ['/admin/user/index']) ?>
                                    <?php } else { ?>
                                        <?= Html::a("Пользователи", ['/admin/user/index']) ?>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                            <li>
                                <?= Html::a('Подразделения', ['/subdivision/index']) ?>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <?= Html::a('CHANGELOG', ['/changelog']) ?>
                            </li>
                            <li>
                                <?= Html::a('Выйти(' . $user->username . ')', ['/site/logout']) ?>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>