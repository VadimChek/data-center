<?php

use frontend\forms\phoneBook\PhoneBookForm;
use frontend\helpers\FilterHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/**
 * @var $this View
 * @var $modelForm PhoneBookForm
 * @var $form ActiveForm
 * @var $showFilter boolean
 */
?>

<div class="panel panel-default">
    <div class="panel-heading search-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        <h4 class="panel-title">
            <a class="accordion-toggle <?= $showFilter ? '' : 'collapsed' ?>" data-toggle="collapse"
               data-parent="#accordion" href="#collapseThree">Форма</a>
        </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse <?= $showFilter ? 'in' : '' ?>"
         data-filter-type="<?= FilterHelper::TYPE_PHONE_BOOK_FILTER ?>">
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'post',
            ]); ?>
            <div class="row">
                <div class="col-sm-2">
                    <?= $form->field($modelForm, 'bold') ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($modelForm, 'name') ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($modelForm, 'phone') ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($modelForm, 'mobile')
                        ->textInput(['placeholder' => '+7(999)999-99-99'])
                        ->widget(MaskedInput::class, [
                            'mask' => '+7(999)999-99-99',
                        ]) ?>
                </div>
                <div class="col-sm-2">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary m-t-25']) ?>
                </div>
            </div>
            <?php $form::end(); ?>
        </div>
    </div>
</div>
