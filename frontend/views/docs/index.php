<?php

use common\models\phoneBook\PhoneBook;
use frontend\assets\jQueryUIAsset;
use frontend\forms\phoneBook\PhoneBookForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $modelForm PhoneBookForm
 * @var $phones PhoneBook[]
 * @var $showFilter boolean
 */

jQueryUIAsset::register($this);
$this->title = 'Телефонная книга';
?>

<h1 class="t-a-c m-t-0">
    <?= h($this->title) ?>
</h1>

<?= $this->render('_search', [
    'modelForm' => $modelForm,
    'showFilter' => $showFilter,
]); ?>

<table class="table table-bordered table-striped bg-white">
    <thead>
    <tr>
        <th></th>
        <th>№</th>
        <th>Название</th>
        <th>Телефонный</th>
        <th>Мобильный</th>
        <th></th>
    </tr>
    </thead>
    <tbody id="phone-book">
    <?php $count = 1 ?>
    <?php foreach ($phones as $key => $phone) { ?>
        <tr data-id="<?= $phone->id ?>">
            <td><i class="fas fa-arrows-alt-v fs-16 text-primary cursor-move handle"></i></td>
            <td><?php if ($phone->name) {
                    echo $count;
                    $count++;
                } ?></td>
            <td><?= $phone->name ?></td>
            <td><?= h($phone->phone) ?></td>
            <td><?= h($phone->mobile) ?></td>
            <td><?= Html::a('<i class="fa fa-trash text-danger"></i>', ['/docs/delete', 'id' => $phone->id], [
                    'data-confirm' => 'Удалить запись?',
                    'title' => 'Удалить',
                    'data' => [
                        'toggle' => 'tooltip',
                        'placement' => 'top',
                    ],
                ]) ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
