<?php

namespace frontend\helpers;

use Yii;

/**
 * Class FilterHelper
 * @package frontend\helpers
 */
class FilterHelper
{
    const TYPE_BID_FILTER = 'bid-filter';
    const TYPE_USER_FILTER = 'user-filter';
    const TYPE_SUBDIVISION_FILTER = 'subdivision-filter';
    const TYPE_STATISTICS_FILTER = 'statistics-filter';
    const TYPE_EVENTS_FILTER = 'events-filter';
    const TYPE_PHONE_BOOK_FILTER = 'phone-book-filter';

    /**
     * @param $type
     *
     * @return mixed
     */
    public static function isShow($type)
    {
        return Yii::$app->cache->get(self::getCacheKey($type));
    }

    /**
     * @param $type
     *
     * @return string
     */
    public static function getCacheKey($type): string
    {
        return 'cache-key-' . $type . '-' . Yii::$app->user->getId();
    }

    /**
     * @param string $type
     * @param string $event
     */
    public static function toggle(string $type, string $event)
    {
        $cacheKey = FilterHelper::getCacheKey($type);
        $event == 'show'
            ? Yii::$app->cache->set($cacheKey, 1)
            : Yii::$app->cache->delete($cacheKey);
    }
}
