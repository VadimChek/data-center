<?php

namespace frontend\helpers;

use Yii;

/**
 * Class CacheHelper
 * @package common\helpers
 */
class CacheHelper
{
    /**
     * @param string $key
     *
     * @return string
     */
    public static function withUserPostfix(string $key): string
    {
        if (!Yii::$app->user->isGuest) {
            $key = "{$key}-" . Yii::$app->user->id;
        }

        return $key;
    }
}
