window.App = {
    successBox: function ($msg) {
        new jBox("Notice", {
            color: 'green',
            content: $msg,
            animation: "flip"
        });
    },
    errorBox: function ($msg) {
        new jBox("Notice", {
            color: 'red',
            content: $msg,
            animation: "flip"
        });
    }
}