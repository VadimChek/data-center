$(document).ready(function () {
  if ($.fn.sortable) {
    $("#phone-book").sortable({
      axis: 'y',
      cursor: 'grabbing',
      handle: '.handle',
      update: function (event, ui) {
        let items = {};
        $(this).find('tr').each(function (i, item) {
          items[i] = $(item).data('id');
        })

        $.ajax({
          url: '/ajax/docs/recalculate-position',
          type: 'POST',
          data: {'items': items},
          success: function (data) {
            if (data.status === 'ok') {
              App.successBox('Сохранено');
            } else {
              App.errorBox(data.message);
            }
          },
          error: function () {
            App.errorBox('Ошибка');
          }
        })
      }
    });
  }

  if ($.fn.wysiwyg) {
    $('#editor').wysiwyg({hotKeys: {}});
  }

  $('#js-note-save').on('click', function (e) {
    e.preventDefault();

    $.ajax({
      url: '/ajax/note/save',
      type: 'POST',
      data: {'content': $('#editor').html()},
      success: function (data) {
        if (data.status === 'ok') {
          App.successBox('Сохранено');
        } else {
          App.errorBox(data.message);
        }
      },
      error: function () {
        App.errorBox('Ошибка');
      }
    })
  });

  $('[data-toggle="tooltip"]').tooltip();

  $('.js-create-event').on('click', function (e) {
    e.preventDefault();
    let $elem = $(this);

    $.ajax({
      url: '/ajax/modals/modal-create-event?date=' + $elem.data('date'),
      type: 'GET',
      success: function (data) {
        let $modal = $('#common-modal-small');
        $modal.find('.modal-content').html(data);
        $modal.modal('show');
      },
      error: function () {
        App.errorBox('Ошибка');
      }
    })
  });

  $('.js-archive-bid').on('click', function (e) {
    e.preventDefault();
    if (!confirm('Вы действительно хотите скрыть выбранные заявки?')) {
      return false;
    }

    let ids = [];
    $('input[name="selection[]"]:checked').each(function () {
      ids.push(this.value);
    });

    if (!ids) {
      return false;
    }

    $.ajax({
      url: '/ajax/bid/hide',
      type: 'POST',
      data: {'ids': ids},
      success: function (data) {
        if (data.status === 'ok') {
          window.location.reload();
        } else {
          App.errorBox(data.message);
        }
      },
      error: function () {
        App.errorBox('Ошибка');
      }
    })
  });

  $('.js-label-content').on('click', function (e) {
    e.preventDefault();
    let $elem = $(this);

    $elem.closest('.form-group').find('.js-input-content').val($elem.text());
  });

  $('.collapse').on('show.bs.collapse', function () {
    saveCollapse('show', $(this).data('filter-type'));
  }).on('hide.bs.collapse', function () {
    saveCollapse('hide', $(this).data('filter-type'));
  });

  function saveCollapse(eventName, filterType) {
    $.ajax({
      url: '/ajax/filter/collapse',
      type: 'POST',
      data: {
        'event': eventName,
        'filter-type': filterType
      },
    })
  }
});