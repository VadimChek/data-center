jQuery(document).ready(function () {
    new jBox.plugin('Notice', {
        color: null,
        stack: true,
        stackSpacing: 10,
        autoClose: 6000,
        attributes: {
            x: 'right',
            y: 'top'
        },
        position: {
            x: 15,
            y: 15
        },
        responsivePositions: {
            500: {
                x: 5,
                y: 5
            },
            768: {
                x: 10,
                y: 10
            }
        },
        target: window,
        fixed: true,
        animation: 'zoomIn',
        closeOnClick: 'box',
        zIndex: 12000,
        _onInit: function ()
        {
            this.defaultNoticePosition = jQuery.extend({}, this.options.position);
            this._adjustNoticePositon = function () {
                var win = jQuery(window);
                var windowDimensions = {
                    x: win.width(),
                    y: win.height()
                };
                this.options.position = jQuery.extend({}, this.defaultNoticePosition);
                jQuery.each(this.options.responsivePositions, function (viewport, position) {
                    if (windowDimensions.x <= viewport) {
                        this.options.position = position;
                        return false;
                    }
                }.bind(this));
                this.options.adjustDistance = {
                    top: this.options.position.y,
                    right: this.options.position.x,
                    bottom: this.options.position.y,
                    left: this.options.position.x
                };
            };
            this.options.content instanceof jQuery && (this.options.content = this.options.content.clone().attr('id', ''));
            jQuery(window).on('resize.responsivejBoxNotice-' + this.id, function (ev) { if (this.isOpen) { this._adjustNoticePositon(); } }.bind(this));
            this.open();
        },
        _onCreated: function ()
        {
            this.wrapper.addClass('jBox-Notice-color jBox-Notice-' + (this.options.color || 'gray'));
            this.wrapper.data('jBox-Notice-position', this.options.attributes.x + '-' + this.options.attributes.y);
        },
        _onOpen: function ()
        {
            this._adjustNoticePositon();
            jQuery.each(jQuery('.jBox-Notice'), function (index, el)
            {
                el = jQuery(el);
                if (el.attr('id') == this.id || el.data('jBox-Notice-position') != this.options.attributes.x + '-' + this.options.attributes.y) return;
                if (!this.options.stack) {
                    el.data('jBox').close({ignoreDelay: true});
                    return;
                }
                var margin = (el.data('jBoxNoticeMargin') ? parseInt(el.data('jBoxNoticeMargin')) : parseInt(el.css('margin-' + this.options.attributes.y))) + this.wrapper.outerHeight() + this.options.stackSpacing;
                el.data('jBoxNoticeMargin', margin);
                el.css('margin-' + this.options.attributes.y, margin);

            }.bind(this));
        },
        _onCloseComplete: function ()
        {
            this.destroy();
        }
    });
});