<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package frontend\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/fontawesome.min.css',
        'css/jBox.css',
        'css/jBox.Notice.css',
        'css/site.css',
        'css/style.css',
    ];

    public $js = [
        'js/jBox.min.js',
        'js/jBox.Notice.js',
        'js/app.funcs.js',
        'js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
