<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class jQueryUIAsset
 * @package frontend\assets
 */
class jQueryUIAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/jquery-ui.min.css',
    ];

    public $js = [
        'js/jquery-ui.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
