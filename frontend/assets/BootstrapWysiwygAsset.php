<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class BootstrapWysiwygAsset
 * @package frontend\assets
 */
class BootstrapWysiwygAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/style.css',
    ];

    public $js = [
        'js/bootstrap-wysiwyg.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
