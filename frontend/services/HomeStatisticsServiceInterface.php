<?php

namespace frontend\services;

/**
 * Interface HomeStatisticsServiceInterface
 * @package frontend\services
 */
interface HomeStatisticsServiceInterface
{
    /**
     * @return int
     */
    public function allCountBid(): int;

    /**
     * @return int
     */
    public function allCountUser(): int;

    /**
     * @param int $departmentId
     *
     * @return int
     */
    public function countBidOnDepartment(int $departmentId): int;

    /**
     * @return int
     */
    public function todayCountEvents(): int;
}
