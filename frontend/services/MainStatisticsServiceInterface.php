<?php

namespace frontend\services;

/**
 * Interface MainStatisticsServiceInterface
 * @package frontend\services
 */
interface MainStatisticsServiceInterface
{
    /**
     * @return array
     */
    public function buildingStat(): array;

    /**
     * @return array
     */
    public function departmentStat(): array;

    /**
     * @return array
     */
    public function subdivisionStat(): array;

    /**
     * @return array
     */
    public function countBidStat(): array;
}
