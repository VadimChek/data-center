<?php

namespace frontend\services\data;

use common\models\data\Data;
use common\models\profile\Profile;
use DomainException;
use frontend\services\DataServiceInterface;
use yii\base\BaseObject;

/**
 * Class DataService
 * @package frontend\services\data
 */
class DataService extends BaseObject implements DataServiceInterface
{
    /**
     * @param string $key
     * @param string $content
     *
     * @return Data
     */
    public function save(string $key, string $content): Data
    {
        $data = Data::getByKey($key) ?? new Data(['key' => $key]);
        $data->content = $content;
        if ($data->save()) {
            return $data;
        }

        throw new DomainException('Ошибка сохранения данных');
    }

    /**
     * @param int $currentUserId
     *
     * @return int
     */
    public function markUnreadNotes(int $currentUserId): int
    {
        return Profile::updateAll(['is_read_notes' => false], ['<>', 'user_id', $currentUserId]);
    }
}
