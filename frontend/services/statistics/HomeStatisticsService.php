<?php

namespace frontend\services\statistics;

use common\models\user\User;
use common\models\bid\Bid;
use common\models\event\Events;
use frontend\services\HomeStatisticsServiceInterface;
use yii\base\BaseObject;

/**
 * Class StatisticsService
 * @package frontend\services\statistics
 */
class HomeStatisticsService extends BaseObject implements HomeStatisticsServiceInterface
{
    /**
     * @return int
     */
    public function allCountBid(): int
    {
        return Bid::find()
            ->select('count(*)')
            ->active()
            ->cache(15)
            ->scalar();
    }

    /**
     * @return int
     */
    public function allCountUser(): int
    {
        return User::find()
            ->select('count(*)')
            ->andWhere(['status' => User::STATUS_ACTIVE])
            ->cache(15)
            ->scalar();
    }

    /**
     * @param int $departmentId
     *
     * @return int
     */
    public function countBidOnDepartment(int $departmentId): int
    {
        return $departmentId
            ? Bid::find()
                ->select('count(*)')
                ->active()
                ->byDepartmentId($departmentId)
                ->cache(15)
                ->scalar()
            : 0;
    }

    /**
     * @return int
     */
    public function todayCountEvents(): int
    {
        return Events::find()
            ->select('count(*)')
            ->active()
            ->byDate(date('Y-m-d'))
            ->cache(15)
            ->scalar();
    }
}
