<?php

namespace frontend\services\statistics;

use common\models\bid\Bid;
use common\models\department\Department;
use common\models\subdivision\Subdivision;
use frontend\models\statistics\DateForm;
use frontend\services\MainStatisticsServiceInterface;
use yii\base\BaseObject;
use yii\db\ActiveQuery;

/**
 * Class MainStatisticsService
 * @package frontend\services\statistics
 */
class MainStatisticsService extends BaseObject implements MainStatisticsServiceInterface
{
    private $dateForm;

    /**
     * MainStatisticsService constructor.
     *
     * @param DateForm $dateForm
     * @param array $config
     */
    public function __construct(DateForm $dateForm, array $config = [])
    {
        $this->dateForm = $dateForm;
        parent::__construct($config);
    }

    /**
     * @param DateForm $dateForm
     */
    public function setDateForm(DateForm $dateForm)
    {
        $this->dateForm = $dateForm;
    }

    /**
     * @return array
     */
    public function buildingStat(): array
    {
        $query = Bid::find()
            ->alias('n')
            ->select(["COALESCE(building, '0') || ' корпус' AS name", 'count(*) as y'])
            ->groupBy('building')
            ->orderBy(['count(*)' => SORT_DESC])
            ->asArray();

        $this->setDateFilter($query);

        return $query->cache(30)->all();
    }

    /**
     * @return array
     */
    public function departmentStat(): array
    {
        $query = Bid::find()
            ->alias('b')
            ->select(["COALESCE(d.name, 'Без отдела') AS name", 'count(*) as y'])
            ->leftJoin(Department::tableName() . ' d', 'b.department_id = d.id')
            ->groupBy('d.name')
            ->orderBy(['count(*)' => SORT_DESC])
            ->asArray();

        $this->setDateFilter($query);

        return $query->cache(30)->all();
    }

    /**
     * @return array
     */
    public function subdivisionStat(): array
    {
        $query = Bid::find()
            ->alias('b')
            ->select(["COALESCE(s.name, 'Без подразделения') AS name", 'count(*) as y'])
            ->leftJoin(Subdivision::tableName() . ' s', 'b.subdivision_id = s.id')
            ->groupBy('s.name')
            ->orderBy(['count(*)' => SORT_DESC])
            ->asArray();

        $this->setDateFilter($query);

        return $query->cache(30)->all();
    }

    /**
     * @return array
     */
    public function countBidStat(): array
    {
        $query = Bid::find()
            ->select(["(to_char(date, 'dd-mm-yyyy'))::date as name", 'count(*) as y'])
            ->groupBy('name')
            ->orderBy(['name' => SORT_ASC])
            ->asArray();

        $this->setDateFilter($query);

        return $query->cache(30)->all();
    }

    /**
     * @param ActiveQuery $query
     */
    private function setDateFilter(ActiveQuery $query)
    {
        $dateFrom = $this->dateForm->dateFrom;
        $dateTo = $this->dateForm->dateTo;

        if ($dateFrom) {
            $query->andWhere(['>=', 'date', date('Y-m-d 00:00:00', strtotime($dateFrom))]);
        }

        if ($dateTo) {
            $query->andWhere(['<=', 'date', date('Y-m-d 23:59:59', strtotime($dateTo))]);
        }
    }
}
