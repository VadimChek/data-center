<?php

namespace frontend\services;

use common\models\data\Data;

/**
 * Interface NoteServiceInterface
 * @package frontend\services
 */
interface DataServiceInterface
{
    /**
     * @param string $key
     * @param string $content
     *
     * @return Data
     */
    public function save(string $key, string $content): Data;

    /**
     * @param int $currentUserId
     *
     * @return mixed
     */
    public function markUnreadNotes(int $currentUserId);
}
