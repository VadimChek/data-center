<?php

namespace frontend\traits;

use Yii;

/**
 * Trait FlashTrait
 * @package frontend\traits
 */
trait FlashTrait
{
    /**
     * @param string $msg
     */
    public function flashSuccess(string $msg = 'Успешно')
    {
        Yii::$app->session->setFlash('success', $msg);
    }

    /**
     * @param string $msg
     */
    public function flashError(string $msg = 'Упс, что-то пошло не так...')
    {
        Yii::$app->session->setFlash('error', $msg);
    }
}
