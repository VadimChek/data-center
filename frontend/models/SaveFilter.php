<?php

namespace frontend\models;

use frontend\helpers\CacheHelper;
use Yii;

/**
 * Trait SaveFilter
 * @package frontend\models
 */
trait SaveFilter
{
    /**
     * @return void
     */
    public function restoreAttributes(): void
    {
        if ($savedAttributes = Yii::$app->cache->get($this->generateCacheKey())) {
            $this->setAttributes($savedAttributes);
        }
    }

    /**
     * @return void
     */
    public function saveAttributes(): void
    {
        Yii::$app->cache->set($this->generateCacheKey(), $this->attributes);
    }

    /**
     * @return string
     */
    public function generateCacheKey(): string
    {
        return CacheHelper::withUserPostfix(self::class);
    }
}
