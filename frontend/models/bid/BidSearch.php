<?php

namespace frontend\models\bid;

use common\models\bid\Bid;
use frontend\models\SaveFilter;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class BidSearch
 * @package frontend\models\bid
 */
class BidSearch extends Model
{
    use SaveFilter;

    public $number;
    public $acceptedUser;
    public $applicantUser;
    public $departmentId;
    public $subdivisionId;
    public $content;
    public $building;
    public $floor;
    public $room;
    public $phone;
    public $status;
    public $dateFrom;
    public $dateTo;
    public $isVisible;

    /**
     * BidSearch constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->restoreAttributes();
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['number', 'building', 'floor', 'status', 'departmentId', 'subdivisionId', 'isVisible'], 'integer'],
            [['acceptedUser', 'applicantUser', 'room', 'phone', 'dateFrom', 'dateTo', 'content'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'number' => 'Номер по порядку',
            'acceptedUser' => 'Кто принял заявку',
            'applicantUser' => 'Заявитель',
            'departmentId' => 'Ответственный',
            'subdivisionId' => 'Подразделение ВИТ ЭРА',
            'content' => 'Содержание заявки',
            'note' => 'Примечание',
            'building' => 'Корпус',
            'floor' => 'Этаж',
            'room' => 'Помещение',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'date' => 'Дата подачи',
            'isVisible' => 'Видимость',
            'dateFrom' => 'Дата подачи от',
            'dateTo' => 'Дата подачи до',
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params = []): ActiveDataProvider
    {
        $query = Bid::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'status' => SORT_ASC,
                    'number' => SORT_DESC,
                ],
                'attributes' => [
                    'id', 'number', 'accepted_user', 'applicant_user', 'department_id', 'building', 'date', 'content',
                    'status', 'subdivision_id'
                ],
            ],
        ]);

        $this->load($params);
        $this->saveAttributes();

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (!$this->isVisible) {
            $this->isVisible = Bid::VISIBLE;
        }

        if ($this->isVisible == Bid::VISIBLE) {
            $query->andWhere(['is_visible' => true]);
        } else if ($this->isVisible == Bid::NOT_VISIBLE) {
            $query->andWhere(['is_visible' => false]);
        }

        if ($this->status && $this->status != Bid::STATUS_ALL) {
            $query->andWhere(['status' => $this->status]);
        }

        if ($this->dateFrom) {
            $query->andWhere(['>=', 'date', date('Y-m-d 00:00:00', strtotime($this->dateFrom))]);
        }

        if ($this->dateTo) {
            $query->andWhere(['<=', 'date', date('Y-m-d 23:59:59', strtotime($this->dateTo))]);
        }

        $query->andFilterWhere([
            'number' => $this->number,
            'building' => $this->building,
            'floor' => $this->floor,
            'department_id' => $this->departmentId,
            'subdivision_id' => $this->subdivisionId,
        ]);

        $query
            ->andFilterWhere(['ilike', 'accepted_user', $this->acceptedUser])
            ->andFilterWhere(['ilike', 'applicant_user', $this->applicantUser])
            ->andFilterWhere(['ilike', 'content', $this->content])
            ->andFilterWhere(['ilike', 'room', $this->room])
            ->andFilterWhere(['ilike', 'phone', $this->phone]);

        return $dataProvider;
    }
}
