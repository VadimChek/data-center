<?php

namespace frontend\models\event;

use common\models\event\Events;
use frontend\models\statistics\DateForm;
use yii\base\Model;

/**
 * Class EventsSearch
 * @package frontend\models\event
 *
 * @property string $name
 * @property integer $type
 */
class EventsSearch extends Model
{
    public $name;
    public $type;

    /** @var DateForm */
    private $dateForm;

    /**
     * EventsSearch constructor.
     *
     * @param DateForm $dateForm
     * @param array $config
     */
    public function __construct(DateForm $dateForm, $config = [])
    {
        $this->dateForm = $dateForm;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name'], 'string'],
            [['type'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'type' => 'Тип',
            'name' => 'Название',
        ];
    }

    /**
     * @param $params
     *
     * @return array
     */
    public function search($params): array
    {
        $query = Events::find()
            ->active()
            ->asArray();

        $dateFrom = $this->dateForm->dateFrom;
        $dateTo = $this->dateForm->dateTo;

        if ($dateFrom) {
            $query->andWhere(['>=', 'date', date('Y-m-d', strtotime($dateFrom))]);
        }

        if ($dateTo) {
            $query->andWhere(['<=', 'date', date('Y-m-d', strtotime($dateTo))]);
        }

        $this->load($params);
        $this->dateForm->saveAttributes();

        $query->andFilterWhere(['ilike', 'name', $this->name]);
        $query->andFilterWhere(['type' => $this->type]);

        $events = [];
        foreach ($query->all() as $event) {
            $events[date('Y-m-d', strtotime($event['date']))][] = [
                'id' => $event['id'],
                'name' => $event['name'],
            ];
        }

        return $events;
    }
}
