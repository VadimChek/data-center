<?php

namespace frontend\models\user;

use common\models\profile\Profile;
use common\models\user\User;
use frontend\models\SaveFilter;
use yii\base\Model;
use yii\data\ArrayDataProvider;

/**
 * Class UserSearch
 * @package frontend\models\user
 *
 * @property string $fio
 * @property string $status
 * @property string $departmentId
 */
class UserSearch extends Model
{
    use SaveFilter;

    public $fio;
    public $status;
    public $departmentId;

    /**
     * UserSearch constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->restoreAttributes();
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['fio'], 'string'],
            [['status', 'departmentId'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'fio' => 'ФИО',
            'status' => 'Статус',
            'departmentId' => 'Отдел',
        ];
    }

    /**
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search(array $params): ArrayDataProvider
    {
        $query = User::find()
            ->alias('u')
            ->innerJoin(Profile::tableName() . ' p', 'p.user_id = u.id')
            ->andWhere(['<>', 'u.id', 1]);

        $this->load($params);
        $this->saveAttributes();

        if (!$this->validate()) {
            $query->where('0=1');
        }

        $query
            ->andFilterWhere(['u.status' => $this->status])
            ->andFilterWhere(['u.department_id' => $this->departmentId])
            ->andFilterWhere(['ilike', "(u.secondname || ' ' || u.firstname || ' ' || u.lastname)", $this->fio]);

        return new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
                'attributes' => [
                    'id', 'fio', 'status', 'department_id'
                ],
            ],
        ]);
    }
}
