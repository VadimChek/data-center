<?php

namespace frontend\models\subdivision;

use common\models\subdivision\Subdivision;
use frontend\models\SaveFilter;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class SubdivisionSearch
 * @package frontend\models\subdivision
 */
class SubdivisionSearch extends Model
{
    use SaveFilter;

    public $status;
    public $name;

    /**
     * SubdivisionSearch constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->restoreAttributes();
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['status'], 'integer'],
            [['name'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'name' => 'Название',
            'status' => 'Статус',
        ];
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = Subdivision::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                    'status' => SORT_ASC,
                ],
                'attributes' => [
                    'id', 'name', 'status',
                ],
            ],
        ]);

        $this->load($params);
        $this->saveAttributes();

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['status' => $this->status]);
        $query->andFilterWhere(['ilike', 'name', $this->name]);

        return $dataProvider;
    }
}
