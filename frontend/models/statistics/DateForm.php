<?php

namespace frontend\models\statistics;

use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use frontend\models\SaveFilter;
use yii\base\Model;

/**
 * Class StatisticsDateForm
 * @package common\models\subdivision
 *
 * @property string $dateFrom
 * @property string $dateTo
 */
class DateForm extends Model
{
    use SaveFilter;

    public $dateFrom;
    public $dateTo;

    /**
     * DateForm constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->restoreAttributes();
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['dateFrom', 'dateTo'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'dateFrom' => 'Дата от',
            'dateTo' => 'Дата до',
        ];
    }

    /**
     * @return DatePeriod
     * @throws \Exception
     */
    public function generatePeriod(): DatePeriod
    {
        $dateFrom = $this->dateFrom;
        $dateTo = $this->dateTo;

        $dateStart = $dateFrom
            ? new DateTimeImmutable(date('Y-m-d 00:00:00', strtotime($dateFrom)))
            : new DateTimeImmutable(date('Y-m-01 00:00:00'));

        $dateEnd = $dateTo
            ? new DateTimeImmutable(date('Y-m-d  23:59:59', strtotime($dateTo)))
            : new DateTimeImmutable(date('Y-m-t 23:59:59'));

        $interval = new DateInterval('P1D');

        return new DatePeriod($dateStart, $interval, $dateEnd);
    }
}
