<?php

namespace frontend\forms;

use common\helpers\DbHelper;
use common\models\profile\Profile;
use common\models\user\User;
use DomainException;
use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * Class SignupForm
 * @package frontend\forms
 *
 * @property string $firstname
 * @property string $secondname
 * @property string $lastname
 * @property integer $role
 * @property string $username
 * @property integer $department_id
 * @property string $password
 * @property string $rPassword
 *
 * @property User $_user
 */
class SignupForm extends Model
{
    public $firstname;
    public $secondname;
    public $lastname;
    public $role;
    public $username;
    public $department_id;
    public $password;
    public $rPassword;

    private $_user;

    /**
     * SignupForm constructor.
     *
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['username', 'firstname', 'secondname', 'lastname'], 'trim'],
            [['username', 'firstname', 'secondname', 'lastname'], 'filter', 'filter' => 'strip_tags'],
            [['username', 'firstname', 'secondname', 'lastname'], 'string', 'min' => 2, 'max' => 255],

            ['role', 'string'],
            ['role', 'required'],

            ['username', 'required'],

            [
                'username',
                'unique',
                'targetClass' => User::class,
                'message' => 'Пользователь с таким логином уже зарегистрирован.'
            ],

            ['password', 'required'],
            ['password', 'string', 'min' => 4, 'max' => 75],

            ['rPassword', 'required'],
            ['rPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают.'],

            [['department_id'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'role' => 'Роль',
            'firstname' => 'Имя',
            'secondname' => 'Фамилия',
            'lastname' => 'Отчество',
            'username' => 'Логин',
            'department_id' => 'Отдел',
            'password' => 'Пароль',
            'rPassword' => 'Повторите пароль'
        ];
    }

    /**
     * @param bool $validation
     *
     * @return User|false
     * @throws Exception
     */
    public function create(bool $validation = true)
    {
        if ($validation && !$this->validate()) {
            return false;
        }

        $user = $this->_user;
        $user->setAttributes($this->getAttributes(null, ['role', 'password', 'rPassword']));
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return DbHelper::transactional(function () use ($user) {
            if (!$user->save()) {
                throw new DomainException('Ошибка регистрации');
            }

            $profile = new Profile(['user_id' => $user->id]);
            if (!$profile->save()) {
                throw new DomainException('Ошибка создания профиля пользователя');
            }

            $role = Yii::$app->authManager->getRole($this->role);
            Yii::$app->authManager->assign($role, $user->id);

            return $user;
        });
    }
}
