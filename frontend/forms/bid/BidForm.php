<?php

namespace frontend\forms\bid;

use common\helpers\DbHelper;
use common\models\bid\Bid;
use common\services\StatusHistoryServiceInterface;
use DomainException;
use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * Class BidForm
 * @package frontend\forms\bid
 */
class BidForm extends Model
{
    public $number;
    public $accepted_user;
    public $applicant_user;
    public $department_id;
    public $subdivision_id;
    public $content;
    public $note;
    public $building;
    public $floor;
    public $room;
    public $phone;
    public $status;
    public $date;

    private $_bid;
    private $_statusHistoryService;

    /**
     * BidForm constructor.
     *
     * @param Bid|null $bid
     * @param StatusHistoryServiceInterface $statusHistoryService
     * @param array $config
     */
    public function __construct(Bid $bid, StatusHistoryServiceInterface $statusHistoryService, $config = [])
    {
        $this->_statusHistoryService = $statusHistoryService;
        $this->_bid = $bid;
        $this->setAttributes($bid->getAttributes(), false);

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'number' => 'Номер по порядку',
            'accepted_user' => 'Кто принял заявку',
            'applicant_user' => 'Заявитель',
            'department_id' => 'Ответственный',
            'subdivision_id' => 'Подразделение ВИТ ЭРА',
            'content' => 'Содержание заявки',
            'note' => 'Примечание',
            'building' => 'Корпус',
            'floor' => 'Этаж',
            'room' => 'Помещение',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'date' => 'Дата подачи',
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['number', 'content', 'accepted_user', 'department_id', 'subdivision_id', 'applicant_user'], 'required'],

            [['building'], 'integer', 'min' => 1, 'max' => 20],
            [['floor'], 'integer', 'min' => 1, 'max' => 10],

            [['number', 'status', 'department_id', 'subdivision_id'], 'integer'],
            [['content', 'note', 'room'], 'string'],
            [['date'], 'safe'],
            [['accepted_user', 'applicant_user', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @param bool $validation
     *
     * @return false|Bid
     * @throws Exception
     */
    public function save($validation = true)
    {
        if ($validation && !$this->validate()) {
            return false;
        }

        $bid = $this->_bid;
        $bid->setAttributes($this->getAttributes());
        if ($bid->isNewRecord) {
            $bid->number = $this->generateNumber();
        }

        return DbHelper::transactional(function () use ($bid) {
            if (!$bid->isNewRecord) {
                $this->_statusHistoryService->createFromBid($bid, Yii::$app->user->getId());
            }

            if (!$bid->save()) {
                throw new DomainException('Ошибка сохранения заявки');
            }

            return $bid;
        });
    }

    /**
     * @return bool|int|mixed|string|null
     */
    public function generateNumber()
    {
        return Bid::find()->max('number') + 1;
    }

    /**
     * @return $this
     */
    public function setNumber(): BidForm
    {
        $this->number = $this->generateNumber();
        return $this;
    }

    /**
     * @return $this
     */
    public function setDate(): BidForm
    {
        $this->date = date('Y-m-d H:i');
        return $this;
    }
}
