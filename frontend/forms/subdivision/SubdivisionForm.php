<?php

namespace frontend\forms\subdivision;

use common\models\subdivision\Subdivision;
use DomainException;
use yii\base\Model;

/**
 * Class SubdivisionForm
 * @package frontend\forms\subdivision
 */
class SubdivisionForm extends Model
{
    public $name;
    public $status;

    private $_subdivision;

    /**
     * SubdivisionForm constructor.
     *
     * @param Subdivision $subdivision
     * @param array $config
     */
    public function __construct(Subdivision $subdivision, $config = [])
    {
        $this->_subdivision = $subdivision;
        $this->setAttributes($subdivision->getAttributes());

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'name' => 'Название',
            'status' => 'Статус',
        ];
    }

    /**
     * @param bool $validation
     *
     * @return Subdivision|false
     */
    public function save($validation = true)
    {
        if ($validation && !$this->validate()) {
            return false;
        }

        $subdivision = $this->_subdivision;
        $subdivision->setAttributes($this->getAttributes(), false);
        if (!$subdivision->save()) {
            throw new DomainException('Ошибка сохранения подразделения. Обратитесь к администратору');
        }

        return $subdivision;
    }
}
