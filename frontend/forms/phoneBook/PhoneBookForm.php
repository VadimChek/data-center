<?php

namespace frontend\forms\phoneBook;

use common\models\phoneBook\PhoneBook;
use DomainException;
use yii\base\Model;

/**
 * Class PhoneBookForm
 * @package frontend\forms\phoneBook
 */
class PhoneBookForm extends Model
{
    public $bold;
    public $name;
    public $phone;
    public $mobile;

    private $_phoneBook;

    /**
     * PhoneBookForm constructor.
     *
     * @param PhoneBook $phoneBook
     * @param array $config
     */
    public function __construct(PhoneBook $phoneBook, $config = [])
    {
        $this->_phoneBook = $phoneBook;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['bold', 'name', 'phone', 'mobile'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'bold' => 'Префикс',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'mobile' => 'Мобильный телефон',
        ];
    }

    /**
     * @param bool $validation
     *
     * @return PhoneBook|false
     */
    public function save($validation = true)
    {
        if ($validation && !$this->validate()) {
            return false;
        }

        $phoneBook = $this->_phoneBook;
        $phoneBook->setAttributes($this->getAttributes(null, ['bold', 'name']));
        $phoneBook->name = "<b>{$this->bold}</b> {$this->name}";
        if (!$phoneBook->save()) {
            throw new DomainException('Ошибка сохранения записи. Обратитесь к администратору');
        }

        return $phoneBook;
    }
}
