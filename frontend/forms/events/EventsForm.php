<?php

namespace frontend\forms\events;

use common\models\event\Events;
use DomainException;
use yii\base\Model;

/**
 * Class EventsForm
 * @package frontend\forms\events
 */
class EventsForm extends Model
{
    public $date;
    public $name;
    public $type;

    private $_event;

    /**
     * EventsForm constructor.
     *
     * @param Events $event
     * @param string|null $date
     * @param array $config
     */
    public function __construct(Events $event, string $date = null, $config = [])
    {
        $this->date = $date;
        $this->_event = $event;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name', 'date'], 'required'],
            [['name', 'date'], 'string'],
            [['type'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'date' => 'Дата',
            'name' => 'Событие',
            'type' => 'Тип',
        ];
    }

    /**
     * @param bool $validation
     *
     * @return Events|false
     */
    public function save($validation = true)
    {
        if ($validation && !$this->validate()) {
            return false;
        }

        $event = $this->_event;
        $event->setAttributes($this->getAttributes(['date', 'name', 'type']), false);
        if (!$event->save(false)) {
            throw new DomainException('Ошибка сохранения события. Обратитесь к администратору');
        }

        return $event;
    }
}
