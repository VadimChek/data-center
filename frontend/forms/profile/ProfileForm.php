<?php

namespace frontend\forms\profile;

use common\helpers\DbHelper;
use common\models\profile\Profile;
use common\models\user\User;
use DomainException;
use yii\base\Model;

/**
 * Class ProfileForm
 * @package frontend\forms\profile
 *
 * @property string $firstname
 * @property string $secondname
 * @property string $lastname
 * @property string $username
 * @property string $phone
 * @property integer $department_id
 * @property string $newPassword
 * @property string $rNewPassword
 */
class ProfileForm extends Model
{
    public $firstname;
    public $secondname;
    public $lastname;
    public $username;
    public $phone;
    public $department_id;
    public $newPassword;
    public $rNewPassword;

    private $user;

    /**
     * ProfileForm constructor.
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->user = $user;
        $this->setAttributes($user->getAttributes());
        $this->checkProfile($user);
        $user->refresh();

        $this->setAttributes($user->profile->getAttributes(), false);
        parent::__construct($config);
    }

    /**
     * @param User $user
     *
     * @return void
     */
    private function checkProfile(User $user): void
    {
        if (!$user->profile) {
            $profile = new Profile(['user_id' => $user->id]);
            if (!$profile->save()) {
                throw new DomainException('Ошибка сохранения данных профиля пользователя');
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'firstname' => 'Имя',
            'secondname' => 'Фамилия',
            'lastname' => 'Отчество',
            'username' => 'Логин',
            'phone' => 'Номер телефона',
            'department_id' => 'Отдел',
            'newPassword' => 'Новый пароль',
            'rNewPassword' => 'Новый пароль',
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['username'], 'required'],
            [['department_id'], 'integer'],
            [
                ['phone', 'firstname', 'secondname', 'lastname', 'username', 'newPassword', 'rNewPassword'],
                'string', 'max' => 255
            ],
            [['newPassword', 'rNewPassword'], 'string', 'min' => 4, 'max' => 75],
            ['newPassword', 'validateComparePassword'],
        ];
    }

    /**
     * @return void
     */
    public function validateComparePassword(): void
    {
        if ($this->newPassword && ($this->newPassword !== $this->rNewPassword)) {
            $this->addError('rNewPassword', "Пароли не совпадают");
        }
    }

    /**
     * @param bool $validation
     *
     * @return false|mixed
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function save($validation = true)
    {
        if ($validation && !$this->validate()) {
            return false;
        }

        $user = $this->user;
        $user->setAttributes($this->getAttributes());
        if ((bool)$this->newPassword) {
            $user->setPassword($this->newPassword);
        }

        $profile = $user->profile;
        $profile->setAttributes($this->getAttributes(['phone']));

        return DbHelper::transactional(function () use ($user, $profile) {
            if (!$user->save() || !$profile->save()) {
                throw new DomainException('Ошибка обновления личных данных. Обратитесь в службу поддержки');
            }

            return $profile;
        });
    }
}
