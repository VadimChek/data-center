<?php

namespace frontend\forms\profile;

use common\helpers\DbHelper;
use common\models\user\User;
use DomainException;
use Yii;
use yii\base\Model;

/**
 * Class AdminProfileForm
 * @package frontend\forms\profile
 *
 * @property string $firstname
 * @property string $secondname
 * @property string $lastname
 * @property string $username
 * @property string $phone
 * @property integer $department_id
 * @property string $role
 *
 * @property string $newPassword
 * @property string $rNewPassword
 *
 * @property User $_user
 */
class AdminProfileForm extends Model
{
    public $firstname;
    public $secondname;
    public $lastname;
    public $username;
    public $phone;
    public $department_id;
    public $role;
    public $newPassword;
    public $rNewPassword;

    private $_user;

    /**
     * ProfileForm constructor.
     *
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        $this->setAttributes($user->getAttributes());
        $this->setAttributes($user->profile->getAttributes());
        $this->role = $this->_user->getRole();

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'firstname' => 'Имя',
            'secondname' => 'Фамилия',
            'lastname' => 'Отчество',
            'username' => 'Логин',
            'phone' => 'Номер телефона',
            'department_id' => 'Отдел',
            'role' => 'Роль',
            'newPassword' => 'Новый пароль',
            'rNewPassword' => 'Новый пароль',
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [
                ['username', 'firstname', 'secondname', 'lastname', 'phone', 'role', 'newPassword', 'rNewPassword'],
                'trim'
            ],
            [['username', 'firstname', 'secondname', 'lastname'], 'filter', 'filter' => 'strip_tags'],
            [['username', 'firstname', 'secondname', 'lastname', 'phone'], 'string', 'min' => 2, 'max' => 255],

            ['role', 'string', 'max' => 255],
            ['role', 'required'],

            ['username', 'required'],

            [
                'username',
                'unique',
                'targetClass' => User::class,
                'filter' => ['<>', 'id', $this->_user->id],
                'message' => 'Пользователь с таким логином уже зарегистрирован.'
            ],

            [['newPassword', 'rNewPassword'], 'string', 'min' => 4, 'max' => 75],
            [['newPassword'], 'validateComparePassword'],

            [['department_id'], 'integer'],
        ];
    }

    /**
     * @return void
     */
    public function validateComparePassword()
    {
        if ($this->newPassword && ($this->newPassword !== $this->rNewPassword)) {
            $this->addError('rNewPassword', 'Пароли не совпадают');
        }
    }

    /**
     * @param bool $validation
     *
     * @return User|false
     * @throws \yii\base\Exception
     */
    public function save($validation = true)
    {
        if ($validation && !$this->validate()) {
            return false;
        }

        $user = $this->_user;
        $user->setAttributes($this->getAttributes(null, ['role', 'password', 'rPassword']));
        $user->profile->setAttributes($this->getAttributes(['phone']));

        if ((bool)$this->newPassword) {
            $user->setPassword($this->newPassword);
        }

        return DbHelper::transactional(function () use ($user) {
            if (!$user->save() || !$user->profile->save()) {
                throw new DomainException('Ошибка обновления данных');
            }

            $role = Yii::$app->authManager->getRole($this->role);
            Yii::$app->authManager->revokeAll($user->id);
            Yii::$app->authManager->assign($role, $user->id);

            return $user;
        });
    }
}
