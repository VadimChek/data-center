<?php

namespace frontend\config\bootstrap;

use common\models\rbac\AuthItem;
use frontend\services\HomeStatisticsServiceInterface;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\web\View;

/**
 * Class ViewBootstrap
 * @package frontend\config\bootstrap
 */
class ViewBootstrap implements BootstrapInterface
{
    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        $this->global();
        $this->header();
    }

    /**
     * @return void
     */
    protected function global(): void
    {
        Event::on(View::class, View::EVENT_BEFORE_RENDER, function () {
            Yii::$app->view->params['global-is-guest'] = Yii::$app->user->isGuest;
            Yii::$app->view->params['global-is-admin'] = Yii::$app->user->can(AuthItem::ROLE_ADMIN);
        });
    }

    /**
     * @return void
     */
    protected function header(): void
    {
        Event::on(View::class, View::EVENT_BEFORE_RENDER, function () {
            $user = Yii::$app->user->identity;
            $bidCount = 0;
            $userCount = 0;
            $todayCountEvents = 0;
            if ($user) {
                $statisticsService = Yii::createObject(HomeStatisticsServiceInterface::class);
                $userCount = $statisticsService->allCountUser();
                $todayCountEvents = $statisticsService->todayCountEvents();

                if ($user->isAdmin() || $user->inDepartmentCod() || !$user->department_id) {
                    $bidCount = $statisticsService->allCountBid();
                } else {
                    $bidCount = $statisticsService->countBidOnDepartment($user->department_id);
                }
            }

            Yii::$app->view->params['statistics-bid-count'] = $bidCount;
            Yii::$app->view->params['statistics-user-count'] = $userCount;
            Yii::$app->view->params['statistics-today-count-events'] = $todayCountEvents;
        });
    }
}
