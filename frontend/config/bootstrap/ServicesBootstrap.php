<?php

namespace frontend\config\bootstrap;

use frontend\services\data\DataService;
use frontend\services\DataServiceInterface;
use frontend\services\HomeStatisticsServiceInterface;
use frontend\services\MainStatisticsServiceInterface;
use frontend\services\statistics\HomeStatisticsService;
use frontend\services\statistics\MainStatisticsService;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class ServicesBootstrap
 * @package common\config\bootstrap
 */
class ServicesBootstrap implements BootstrapInterface
{
    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        $container = Yii::$container;

        $container->set(DataServiceInterface::class, function () {
            return Yii::createObject(DataService::class);
        });

        $container->set(MainStatisticsServiceInterface::class, function () {
            return Yii::createObject(MainStatisticsService::class);
        });

        $container->set(HomeStatisticsServiceInterface::class, function () {
            return Yii::createObject(HomeStatisticsService::class);
        });
    }
}
