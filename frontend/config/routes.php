<?php

return [
    '/' => '/site/index',
    'logout' => 'site/logout',
    'login' => 'site/login',
    'signup' => 'site/signup',
    'profile' => 'user/profile',
    'changelog' => 'site/changelog',
    'note' => 'note/index',

    'bid' => 'bid/index',
    'statistics' => 'statistics/index',

    'admin/user' => 'admin/user/index',
];
