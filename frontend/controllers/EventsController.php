<?php

namespace frontend\controllers;

use common\models\event\Events;
use common\models\rbac\AuthItem;
use common\services\SoftDeleteServiceInterface;
use DomainException;
use frontend\forms\events\EventsForm;
use frontend\helpers\FilterHelper;
use frontend\models\statistics\DateForm;
use frontend\models\event\EventsSearch;
use frontend\traits\FlashTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class EventsController
 * @package frontend\controllers\admin
 */
class EventsController extends FrontendController
{
    use FlashTrait;

    private $_softDeleteService;

    /**
     * EventsController constructor.
     *
     * @param $id
     * @param $module
     * @param SoftDeleteServiceInterface $softDeleteService
     * @param array $config
     */
    public function __construct(
        $id,
        $module,
        SoftDeleteServiceInterface $softDeleteService,
        $config = []
    )
    {
        $this->_softDeleteService = $softDeleteService;
        $this->modelSearchClass = DateForm::class;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'clear-filter', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function actionIndex(): string
    {
        $this->view->params['layout-classes'] = 'w-1500';

        $dateForm = new DateForm();
        $dateForm->load(Yii::$app->request->queryParams);
        $searchModel = Yii::createObject(EventsSearch::class, ['dateForm' => $dateForm]);

        return $this->render('index', [
            'currentDay' => date('Y-m-d'),
            'dateForm' => $dateForm,
            'events' => $searchModel->search(Yii::$app->request->queryParams),
            'period' => $dateForm->generatePeriod(),
            'searchModel' => $searchModel,
            'showFilter' => FilterHelper::isShow(FilterHelper::TYPE_EVENTS_FILTER),
        ]);
    }

    /**
     * @return Response
     * @throws InvalidConfigException
     */
    public function actionCreate(): Response
    {
        $modelForm = Yii::createObject(EventsForm::class);
        try {
            if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
                $this->flashSuccess();
            } else {
                throw new DomainException('Ошибка сохранения события');
            }
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Throwable
     */
    public function actionDelete($id): Response
    {
        try {
            $this->_softDeleteService->softDelete($id, Events::class);
            $this->flashSuccess();
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return Events|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id): ?Events
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
