<?php

namespace frontend\controllers\ajax;

use frontend\forms\events\EventsForm;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class ModalsController
 * @package frontend\controllers\ajax
 */
class ModalsController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['modal-create-event'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $date
     *
     * @return string
     * @throws InvalidConfigException
     */
    public function actionModalCreateEvent($date): string
    {
        return $this->renderAjax("@frontend/views/modals/create-event", [
            'model' => Yii::createObject(EventsForm::class, ['date' => $date]),
        ]);
    }
}
