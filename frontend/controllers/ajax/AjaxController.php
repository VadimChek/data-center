<?php

namespace frontend\controllers\ajax;

use Yii;
use yii\base\Action;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\controllers\ajax
 */
class AjaxController extends Controller
{
    /**
     * @param Action $action
     *
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function successResponse(array $data = []): array
    {
        return array_merge(['status' => 'ok'], $data);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function errorResponse(array $data = []): array
    {
        return array_merge(['status' => 'fail'], $data);
    }
}
