<?php

namespace frontend\controllers\ajax;

use common\services\PhoneBookServiceInterface;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;

/**
 * Class DocsController
 * @package frontend\controllers\ajax
 */
class DocsController extends AjaxController
{
    private $_phoneBookService;

    /**
     * DocsController constructor.
     *
     * @param $id
     * @param $module
     * @param PhoneBookServiceInterface $service
     * @param array $config
     */
    public function __construct($id, $module, PhoneBookServiceInterface $service, $config = [])
    {
        $this->_phoneBookService = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['recalculate-position'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionRecalculatePosition(): array
    {
        $items = Yii::$app->request->post('items');
        if (!is_array($items)) {
            throw new BadRequestHttpException('Неверный запрос');
        }
        $this->_phoneBookService->recalculatePosition($items);

        return $this->successResponse();
    }
}
