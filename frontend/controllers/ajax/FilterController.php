<?php

namespace frontend\controllers\ajax;

use frontend\helpers\FilterHelper;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Class FilterController
 * @package frontend\controllers\ajax
 */
class FilterController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['collapse'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return bool
     * @throws BadRequestHttpException
     */
    public function actionCollapse(): bool
    {
        $event = Yii::$app->request->post('event');
        $type = Yii::$app->request->post('filter-type');
        if ($type && $event) {
            FilterHelper::toggle($type, $event);
            return true;
        }

        throw new BadRequestHttpException('Неверный запрос');
    }
}
