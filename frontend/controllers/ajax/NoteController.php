<?php

namespace frontend\controllers\ajax;

use common\models\data\Data;
use DomainException;
use frontend\services\DataServiceInterface;
use Yii;
use yii\filters\AccessControl;

/**
 * Class NoteController
 * @package frontend\controllers\ajax
 */
class NoteController extends AjaxController
{
    private $_dataService;

    /**
     * NoteController constructor.
     *
     * @param $id
     * @param $module
     * @param DataServiceInterface $service
     * @param array $config
     */
    public function __construct($id, $module, DataServiceInterface $service, $config = [])
    {
        $this->_dataService = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['save'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionSave(): array
    {
        try {
            $this->_dataService->save(Data::NOTE_KEY, Yii::$app->request->post('content'));
            $this->_dataService->markUnreadNotes(Yii::$app->user->id);
        } catch (DomainException $ex) {
            return $this->errorResponse(['message' => $ex->getMessage()]);
        }

        return $this->successResponse();
    }
}
