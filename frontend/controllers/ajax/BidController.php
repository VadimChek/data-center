<?php

namespace frontend\controllers\ajax;

use common\models\rbac\AuthItem;
use common\services\BidServiceInterface;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;

/**
 * Class BidController
 * @package frontend\controllers\ajax
 */
class BidController extends AjaxController
{
    private $_bidService;

    /**
     * BidController constructor.
     *
     * @param $id
     * @param $module
     * @param BidServiceInterface $service
     * @param array $config
     */
    public function __construct($id, $module, BidServiceInterface $service, $config = [])
    {
        $this->_bidService = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['hide'],
                        'allow' => true,
                        'roles' => [AuthItem::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionHide(): array
    {
        try {
            if ($ids = Yii::$app->request->post('ids')) {
                $this->_bidService->hideByIds($ids);
            }
        } catch (Exception $ex) {
            return $this->errorResponse();
        }

        return $this->successResponse();
    }
}
