<?php

namespace frontend\controllers;

use common\models\rbac\AuthItem;
use common\models\bid\Bid;
use common\services\BidServiceInterface;
use DomainException;
use frontend\forms\bid\BidForm;
use frontend\helpers\FilterHelper;
use frontend\traits\FlashTrait;
use frontend\models\bid\BidSearch;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class BidController
 * @package frontend\controllers
 */
class BidController extends FrontendController
{
    use FlashTrait;

    private $bidService;

    /**
     * BidController constructor.
     *
     * @param $id
     * @param $module
     * @param BidServiceInterface $service
     * @param array $config
     */
    public function __construct(
        $id,
        $module,
        BidServiceInterface $service,
        $config = []
    )
    {
        $this->bidService = $service;
        $this->modelSearchClass = BidSearch::class;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'update', 'create', 'check', 'work', 'clear-filter'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['archive'],
                        'allow' => true,
                        'roles' => [AuthItem::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new BidSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'showFilter' => FilterHelper::isShow(FilterHelper::TYPE_BID_FILTER),
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id): string
    {
        $model = Bid::find()->with('statusHistory')->andWhere(['id' => $id])->one();
        if ($model === null) {
            throw new NotFoundHttpException('Заявка не найдена');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|Response
     * @throws Exception
     */
    public function actionCreate()
    {
        $modelForm = Yii::createObject(BidForm::class);
        $modelForm->setDate()->setNumber();
        try {
            if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
                $this->flashSuccess('Заявка создана');
                return $this->redirect(['index']);
            }
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->render('create', [
            'modelForm' => $modelForm,
            'lastAcceptedUser' => Bid::getLastAcceptedUser(),
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelForm = Yii::createObject(BidForm::class, ['bid' => $model]);
        try {
            if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
                $this->flashSuccess('Заявка обновлена');
                return $this->redirect(['index']);
            }
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->render('update', [
            'model' => $model,
            'modelForm' => $modelForm,
            'lastAcceptedUser' => Bid::getLastAcceptedUser(),
        ]);
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function actionArchive(int $id): Response
    {
        try {
            $this->bidService->changeStatus($id, Bid::STATUS_ARCHIVE);
            $this->flashSuccess('Заявка помещена в архив');
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionCheck($id): Response
    {
        try {
            $this->bidService->changeStatus($id, Bid::STATUS_COMPLETE);
            $this->flashSuccess('Заявка исполнена');
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionWork($id): Response
    {
        try {
            $this->bidService->changeStatus($id, Bid::STATUS_IN_WORK);
            $this->flashSuccess('Заявка принята в работу');
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return Bid|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id): ?Bid
    {
        if (($model = Bid::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
