<?php

namespace frontend\controllers;

use frontend\helpers\FilterHelper;
use frontend\models\statistics\DateForm;
use frontend\services\MainStatisticsServiceInterface;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;

/**
 * Class StatisticsController
 * @package frontend\controllers
 */
class StatisticsController extends FrontendController
{
    private $_mainStatisticsService;

    /**
     * StatisticsController constructor.
     *
     * @param $id
     * @param $module
     * @param MainStatisticsServiceInterface $mainStatisticsService
     * @param array $config
     */
    public function __construct(
        $id,
        $module,
        MainStatisticsServiceInterface $mainStatisticsService,
        $config = []
    )
    {
        $this->_mainStatisticsService = $mainStatisticsService;
        $this->modelSearchClass = DateForm::class;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'clear-filter'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function actionIndex(): string
    {
        $dateForm = Yii::createObject(DateForm::class);
        $dateForm->load(Yii::$app->request->queryParams);
        $this->_mainStatisticsService->setDateForm($dateForm);

        return $this->render('index', [
            'dateForm' => $dateForm,
            'showFilter' => FilterHelper::isShow(FilterHelper::TYPE_STATISTICS_FILTER),
            'countBidStat' => $this->_mainStatisticsService->countBidStat(),
            'buildingStat' => $this->_mainStatisticsService->buildingStat(),
            'departmentStat' => $this->_mainStatisticsService->departmentStat(),
            'subdivisionStat' => $this->_mainStatisticsService->subdivisionStat(),
        ]);
    }
}
