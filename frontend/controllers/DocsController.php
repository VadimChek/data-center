<?php

namespace frontend\controllers;

use common\models\phoneBook\PhoneBook;
use common\services\SoftDeleteServiceInterface;
use DomainException;
use frontend\forms\phoneBook\PhoneBookForm;
use frontend\helpers\FilterHelper;
use frontend\traits\FlashTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class DocsController
 * @package frontend\controllers
 */
class DocsController extends Controller
{
    use FlashTrait;

    private $_softDeleteService;

    /**
     * DocsController constructor.
     *
     * @param $id
     * @param $module
     * @param SoftDeleteServiceInterface $softDeleteService
     * @param array $config
     */
    public function __construct(
        $id,
        $module,
        SoftDeleteServiceInterface $softDeleteService,
        $config = []
    )
    {
        $this->_softDeleteService = $softDeleteService;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|Response
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        $modelForm = Yii::createObject(PhoneBookForm::class);
        try {
            if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
                $this->flashSuccess();
                return $this->redirect(['index']);
            }
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->render('index', [
            'modelForm' => $modelForm,
            'phones' => PhoneBook::getAllPhones(),
            'showFilter' => FilterHelper::isShow(FilterHelper::TYPE_PHONE_BOOK_FILTER),
        ]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionDelete($id): Response
    {
        try {
            $this->_softDeleteService->softDelete($id, PhoneBook::class);
            $this->flashSuccess();
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->redirect(['index']);
    }
}
