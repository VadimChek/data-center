<?php

namespace frontend\controllers;

use common\forms\login\LoginForm;
use common\models\rbac\AuthItem;
use frontend\forms\SignupForm;
use frontend\models\event\EventsSearch;
use frontend\models\statistics\DateForm;
use yii\base\Exception;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class SiteController
 * @package frontend\controllers
 */
class SiteController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup', 'index', 'login', 'changelog'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index', 'changelog'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex(): string
    {
        $this->view->params['layout-classes'] = 'w-1500';

        $dateForm = new DateForm([
            'dateFrom' => date('Y-m-d', strtotime('last week monday')),
            'dateTo' => date('Y-m-d', strtotime('next week sunday')),
        ]);
        $searchModel = Yii::createObject(EventsSearch::class, ['dateForm' => $dateForm]);

        return $this->render('index', [
            'currentDay' => date('Y-m-d'),
            'events' => $searchModel->search(Yii::$app->request->queryParams),
            'period' => $dateForm->generatePeriod(),
        ]);
    }

    /**
     * @return string
     */
    public function actionChangelog(): string
    {
        return $this->render('changelog');
    }

    /**
     * @return string|Response
     *
     * @throws InvalidConfigException
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $modelForm = Yii::createObject(LoginForm::class);
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->login()) {
            return $this->goBack();
        }

        $modelForm->password = '';

        return $this->render('login', [
            'model' => $modelForm,
        ]);
    }

    /**
     * @return Response
     */
    public function actionLogout(): Response
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * @return string|Response
     * @throws Exception
     */
    public function actionSignup()
    {
        if (Yii::$app->params['close-registration']) {
            throw new NotFoundHttpException('Страница регистрации недоступна');
        }

        $modelForm = Yii::createObject(SignupForm::class);
        if ($modelForm->load(Yii::$app->request->post())) {
            $modelForm->role = AuthItem::ROLE_OPERATOR;
            if ($user = $modelForm->create()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $modelForm,
        ]);
    }
}
