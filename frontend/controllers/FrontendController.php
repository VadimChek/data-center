<?php

namespace frontend\controllers;

use frontend\helpers\CacheHelper;
use Yii;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class FrontendController
 * @package frontend\controllers
 */
class FrontendController extends Controller
{
    protected $modelSearchClass = 'empty-search-class';

    /**
     * @param string $redirectTo
     *
     * @return Response
     */
    public function actionClearFilter(string $redirectTo = 'index'): Response
    {
        Yii::$app->cache->delete(CacheHelper::withUserPostfix($this->modelSearchClass));

        return $this->redirect([$redirectTo]);
    }
}
