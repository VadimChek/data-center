<?php

namespace frontend\controllers;

use common\services\ProfileServiceInterface;
use common\models\data\Data;
use DomainException;
use frontend\traits\FlashTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class NoteController
 * @package frontend\controllers
 */
class NoteController extends Controller
{
    use FlashTrait;

    private $_profileService;

    /**
     * NoteController constructor.
     *
     * @param $id
     * @param $module
     * @param ProfileServiceInterface $service
     * @param array $config
     */
    public function __construct(
        $id,
        $module,
        ProfileServiceInterface $service,
        $config = []
    )
    {
        $this->_profileService = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'read'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index', [
            'content' => Data::getValueByKey(Data::NOTE_KEY),
        ]);
    }

    /**
     * @return Response
     */
    public function actionRead(): Response
    {
        try {
            $this->_profileService->update(Yii::$app->user->id, ['is_read_notes' => true]);
            $this->flashSuccess();
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->redirect(['index']);
    }
}
