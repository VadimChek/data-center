<?php

namespace frontend\controllers;

use common\models\subdivision\Subdivision;
use common\services\SubdivisionServiceInterface;
use DomainException;
use frontend\forms\subdivision\SubdivisionForm;
use frontend\helpers\FilterHelper;
use frontend\traits\FlashTrait;
use frontend\models\subdivision\SubdivisionSearch;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class SubdivisionController
 * @package frontend\controllers
 */
class SubdivisionController extends FrontendController
{
    use FlashTrait;

    private $_subdivisionService;

    /**
     * SubdivisionController constructor.
     *
     * @param $id
     * @param $module
     * @param SubdivisionServiceInterface $subdivisionService
     * @param array $config
     */
    public function __construct(
        $id,
        $module,
        SubdivisionServiceInterface $subdivisionService,
        $config = []
    )
    {
        $this->_subdivisionService = $subdivisionService;
        $this->modelSearchClass = SubdivisionSearch::class;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'archive', 'restore', 'clear-filter'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new SubdivisionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'showFilter' => FilterHelper::isShow(FilterHelper::TYPE_SUBDIVISION_FILTER),
        ]);
    }

    /**
     * @return string|Response
     * @throws InvalidConfigException
     */
    public function actionCreate()
    {
        $modelForm = Yii::createObject(SubdivisionForm::class);
        try {
            if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
                $this->flashSuccess();
                return $this->redirect(['index']);
            }
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->render('create', [
            'modelForm' => $modelForm,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelForm = Yii::createObject(SubdivisionForm::class, ['subdivision' => $model]);
        try {
            if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
                $this->flashSuccess();
                return $this->redirect(['index']);
            }
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->render('update', [
            'model' => $model,
            'modelForm' => $modelForm,
        ]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionArchive($id): Response
    {
        $this->_subdivisionService->changeStatus($id, Subdivision::STATUS_ARCHIVE)
            ? $this->flashSuccess('Подразделение помещено в архив')
            : $this->flashError('Ошибка помещения подразделения в архив');

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionRestore($id): Response
    {
        $this->_subdivisionService->changeStatus($id, Subdivision::STATUS_ACTIVE)
            ? $this->flashSuccess('Подразделение восстановленно')
            : $this->flashError('Ошибка восстановления подразделения');

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return Subdivision|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id): ?Subdivision
    {
        if (($model = Subdivision::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
