<?php

namespace frontend\controllers\admin;

use common\models\rbac\AuthItem;
use common\models\user\User;
use common\services\UserServiceInterface;
use DomainException;
use frontend\controllers\FrontendController;
use frontend\forms\profile\AdminProfileForm;
use frontend\forms\SignupForm;
use frontend\helpers\FilterHelper;
use frontend\traits\FlashTrait;
use frontend\models\user\UserSearch;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UserController
 * @package frontend\controllers\admin
 */
class UserController extends FrontendController
{
    use FlashTrait;

    private $_userService;

    /**
     * UserController constructor.
     *
     * @param $id
     * @param $module
     * @param UserServiceInterface $service
     * @param array $config
     */
    public function __construct($id, $module, UserServiceInterface $service, $config = [])
    {
        $this->_userService = $service;
        $this->modelSearchClass = UserSearch::class;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'update', 'create', 'delete', 'restore', 'clear-filter'],
                        'allow' => true,
                        'roles' => [AuthItem::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'showFilter' => FilterHelper::isShow(FilterHelper::TYPE_USER_FILTER),
        ]);
    }

    /**
     * @return string|Response
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function actionCreate()
    {
        $modelForm = Yii::createObject(SignupForm::class);
        try {
            if ($modelForm->load(Yii::$app->request->post()) && $modelForm->create()) {
                $this->flashSuccess();
                return $this->redirect(['index']);
            }
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->render('create', [
            'model' => $modelForm,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelForm = Yii::createObject(AdminProfileForm::class, ['user' => $model]);
        try {
            if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
                $this->flashSuccess();
                return $this->redirect(['index']);
            }
        } catch (DomainException $ex) {
            $this->flashError($ex->getMessage());
        }

        return $this->render('update', [
            'model' => $model,
            'modelForm' => $modelForm,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionDelete($id): Response
    {
        if ((int)$id !== 1) {
            $this->_userService->changeStatus($id, User::STATUS_DELETED)
                ? $this->flashSuccess('Пользователь удален с возможностью восстановления')
                : $this->flashError('Ошибка удаления пользователя');
        } else {
            $this->flashError('Данного пользователя нельзя удалить');
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionRestore($id): Response
    {
        $this->_userService->changeStatus($id, User::STATUS_ACTIVE)
            ? $this->flashSuccess('Пользователь восстановлен')
            : $this->flashError('Ошибка восстановления пользователя');

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($id): User
    {
        $model = User::findOne($id);
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
