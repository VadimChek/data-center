<?php

namespace frontend\controllers;

use DomainException;
use frontend\forms\profile\ProfileForm;
use frontend\traits\FlashTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class UserController
 * @package frontend\controllers
 */
class UserController extends Controller
{
    use FlashTrait;

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|Response
     * @throws \Exception
     */
    public function actionProfile()
    {
        $modelForm = Yii::createObject(ProfileForm::class, ['user' => Yii::$app->user->identity]);
        try {
            if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
                $this->flashSuccess();
                return $this->redirect(['user/profile']);
            }
        } catch (DomainException $e) {
            $this->flashError($e->getMessage());
        }

        return $this->render('profile', [
            'modelForm' => $modelForm,
        ]);
    }
}
